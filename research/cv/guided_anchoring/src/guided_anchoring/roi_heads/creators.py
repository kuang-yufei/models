# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
from typing import Optional

from ...config import Config
from ..bbox import create_assigner, create_coder, create_sampler
from ..losses import create_loss_bbox, create_loss_cls
from ..ops import ROIAlign
from .bbox_heads import Shared2FCBBoxHead
from .roi_extractors import SingleRoIExtractor
from .standard_roi_head import StandardRoIHead


def create_bbox_head(config: Config) -> Shared2FCBBoxHead:
    if config.type == 'Shared2FCBBoxHead':
        bbox_coder = create_coder(config.bbox_coder)
        loss_bbox = create_loss_bbox(config.loss_bbox)
        loss_cls = create_loss_cls(config.loss_cls)
        return Shared2FCBBoxHead(
            fc_out_channels=config.fc_out_channels,
            in_channels=config.in_channels,
            bbox_coder=bbox_coder,
            loss_bbox=loss_bbox,
            loss_cls=loss_cls,
            num_classes=config.num_classes,
            reg_class_agnostic=config.reg_class_agnostic,
            roi_feat_size=config.roi_feat_size,
        )
    raise RuntimeError(f'Unknown type: {config.type}')


def create_roi_extractor(config: Config) -> SingleRoIExtractor:
    roi_align = ROIAlign(
        output_size=config.roi_layer.output_size,
        sampling_ratio=config.roi_layer.sampling_ratio,
    )
    return SingleRoIExtractor(
        roi_layer=roi_align,
        out_channels=config.out_channels,
        featmap_strides=config.featmap_strides,
    )


def create_standard_roi_head(
        config: Config,
        test_cfg: Config,
        train_cfg: Optional[Config] = None,
) -> StandardRoIHead:
    bbox_head = create_bbox_head(config.bbox_head)
    roi_extractor = create_roi_extractor(config.bbox_roi_extractor)
    assigner = create_assigner(train_cfg.rcnn.assigner) if train_cfg else None
    sampler = create_sampler(train_cfg.rcnn.sampler) if train_cfg else None
    return StandardRoIHead(
        bbox_roi_extractor=roi_extractor,
        bbox_head=bbox_head,
        assigner=assigner,
        sampler=sampler,
        test_cfg=test_cfg.rcnn.cfg_dict,
        train_cfg=train_cfg.rcnn.cfg_dict if train_cfg else None,
    )
