# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/kakaobrain/nerf-factory
# ============================================================================
"""MindSpore based coordinates sampling: depth and hierarchical samplers."""

import mindspore as ms
import mindspore.nn as nn
from mindspore import ops


def lift_gaussian(d, t_mean, t_var, r_var):

    mean = d[..., None, :] * t_mean[..., None]

    d_mag_sq = ops.ReduceSum(keep_dims=True)(d**2, -1)
    thresholds = ops.OnesLike()(d_mag_sq) * 1e-10
    d_mag_sq = ops.Maximum()(d_mag_sq, thresholds)

    d_outer_diag = d**2
    null_outer_diag = 1 - d_outer_diag / d_mag_sq
    t_cov_diag = t_var[..., None] * d_outer_diag[..., None, :]
    xy_cov_diag = r_var[..., None] * null_outer_diag[..., None, :]
    cov_diag = t_cov_diag + xy_cov_diag

    return mean, cov_diag


# According to the link below, numerically stable implementations are required.
# https://github.com/google/mipnerf/blob/84c969e0a623edd183b75693aed72a7e7c22902d/internal/mip.py#L88
def cast_rays(t_vals, origins, directions, radii, ray_shape):
    t0 = t_vals[..., :-1]
    t1 = t_vals[..., 1:]
    if ray_shape == "cone":     # conical_frustum_to_gaussian
        mu = (t0 + t1) / 2
        hw = (t1 - t0) / 2
        t_mean = mu + (2 * mu * hw ** 2) / (3 * mu ** 2 + hw ** 2)
        t_var = (hw ** 2) / 3 - (4 / 15) * (
            (hw ** 4 * (12 * mu ** 2 - hw ** 2)) / (3 * mu ** 2 + hw ** 2) ** 2
        )
        r_var = radii ** 2 * (
            (mu ** 2) / 4
            + (5 / 12) * hw ** 2
            - 4 / 15 * (hw ** 4) / (3 * mu ** 2 + hw ** 2)
        )
    else:   # ray_shape == "cylinder" cylinder_to_gaussian
        t_mean = (t0 + t1) / 2
        r_var = radii ** 2 / 4
        t_var = (t1 - t0) ** 2 / 12
    means, covs = lift_gaussian(directions, t_mean, t_var, r_var)
    means = means + origins[..., None, :]
    return means, covs


class DepthCoordsBuilder(nn.Cell):

    def __init__(self,
                 num_samples: int,
                 linear_disparity_sampling: bool,
                 perturbation: bool,
                 ray_shape: str,
                 dtype=ms.float32):
        super().__init__()
        self.num_samples = num_samples
        self.linear_disparity_sampling = linear_disparity_sampling
        self.perturbation = perturbation
        self.ray_shape = ray_shape
        self.dtype = dtype

        zero_tensor = ms.Tensor(0.0, dtype=self.dtype)
        one_tensor = ms.Tensor(1.0, dtype=self.dtype)
        self.t_vals = ops.LinSpace()(zero_tensor,
                                     one_tensor,
                                     self.num_samples+1)
        self.concat = ops.Concat(axis=-1)

    def build_coords(self, rays_o, rays_d, radii, near, far):
        if not self.linear_disparity_sampling:
            z_vals = near * (1.0 - self.t_vals) + far * self.t_vals
        else:
            z_vals = 1.0 / (1.0 / near * (1.0 - self.t_vals) +
                            1.0 / far * self.t_vals)
        if self.perturbation and self.training:
            mids = 0.5 * (z_vals[..., 1:] + z_vals[..., :-1])
            upper = self.concat((mids, z_vals[..., -1:]))
            lower = self.concat((z_vals[..., :1], mids))
            t_rand = ms.numpy.rand(z_vals.shape, dtype=self.dtype)
            z_vals = lower + (upper - lower) * t_rand
        means, covs = cast_rays(z_vals, rays_o, rays_d, radii, self.ray_shape)

        return z_vals, (means, covs)


class HierarchicalCoordsBuilder(nn.Cell):

    def __init__(self,
                 hierarchical_samples: int,
                 perturbation: bool,
                 ray_shape: str = 'cone',
                 stop_level_grad: bool = True,
                 resample_padding: float = 0.01,
                 dtype=ms.float32):
        super().__init__()
        self.num_samples = hierarchical_samples+1
        self.perturbation = perturbation
        self.ray_shape = ray_shape
        self.resample_padding = resample_padding
        self.stop_level_grad = stop_level_grad
        self.dtype = dtype

        self.zero_tensor = ms.Tensor(0.0, dtype=self.dtype)
        self.one_tensor = ms.Tensor(1.0, dtype=self.dtype)
        self.linspace = ops.LinSpace()(self.zero_tensor,
                                       self.one_tensor,
                                       self.num_samples)

        self.concat = ops.Concat(axis=-1)
        self.cumsum = ops.CumSum()
        self.max = ops.Maximum()
        self.min = ops.Minimum()
        self.reduce_sum = ops.ReduceSum(keep_dims=True)
        self.eps = 1e-5
        self.float_min_eps = 2**-32
        self.one_init = ms.common.initializer.One()
        self.zero_init = ms.common.initializer.Zero()
        self.max_op = ops.ArgMaxWithValue(axis=-2)
        self.min_op = ops.ArgMinWithValue(axis=-2)
        self.lin_space = ops.LinSpace()

    def build_coords(self, rays_o, rays_d, radii, z_vals, weights):

        weights_pad = self.concat([weights[..., :1], weights, weights[..., -1:]])
        weights_max = self.max(weights_pad[..., :-1], weights_pad[..., 1:])
        weights_blur = 0.5 * (weights_max[..., :-1] + weights_max[..., 1:])
        weights = weights_blur + self.resample_padding

        # pdf
        new_t_vals = self._sorted_piecewise_constant_pdf(z_vals, weights)

        if self.stop_level_grad:
            new_t_vals = ops.stop_gradient(new_t_vals)

        means, covs = cast_rays(new_t_vals, rays_o, rays_d, radii, self.ray_shape)

        return new_t_vals, (means, covs)

    def _sorted_piecewise_constant_pdf(self, bins, weights):

        weight_sum = self.reduce_sum(weights, -1)
        padding = self.max(ops.zeros_like(weight_sum), self.eps - weight_sum)
        weights += padding / weights.shape[-1]
        weight_sum += padding

        pdf = weights / weight_sum
        cdf = self.min(
            ops.ones_like(pdf[..., :-1]), self.cumsum(pdf[..., :-1], -1)
        )
        cdf_zero = ops.zeros_like(cdf[..., :1])
        cdf_one = ops.ones_like(cdf[..., :1])
        cdf = self.concat((cdf_zero, cdf, cdf_one))

        if self.perturbation:
            s = 1 / self.num_samples
            u = self.linspace
            u += ms.numpy.rand(u.shape) * (s - self.float_min_eps)
            u = self.min(u, ops.ones_like(u) * (1.0 - self.float_min_eps))
        else:
            u = self.lin_space(self.zero_tensor,
                               ms.Tensor(1.0 - self.float_min_eps, dtype=self.dtype),
                               self.num_samples)
            u = ops.BroadcastTo(tuple(cdf.shape[:-1]) + (self.num_samples,))(u)

        mask = u[..., None, :] >= cdf[..., :, None]

        bin0 = self.max_op(mask * bins[..., None] + ~mask * bins[..., :1, None])[1]
        bin1 = self.min_op(~mask * bins[..., None] + mask * bins[..., -1:, None])[1]

        cdf0 = self.max_op(mask * cdf[..., None] + ~mask * cdf[..., :1, None])[1]
        cdf1 = self.min_op(~mask * cdf[..., None] + mask * cdf[..., -1:, None])[1]

        t = (u - cdf0) / (cdf1 - cdf0)

        t = ops.clamp(ops.nan_to_num(t, 0.0, 0.0, 0.0), 0, 1)
        samples = bin0 + t * (bin1 - bin0)

        return samples
