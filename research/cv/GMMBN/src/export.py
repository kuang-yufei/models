import models
import numpy as np
from utils.utils import get_args
import mindspore
from mindspore import Tensor

mindspore.set_context(mode=mindspore.PYNATIVE_MODE, device_target='Ascend', device_id=0)
args = get_args()
model = models.create_model(args)
param_dict = mindspore.load_checkpoint(args.export_model)
param_not_load = mindspore.load_param_into_net(model, param_dict)
print(param_not_load)
inputs = Tensor(np.ones([1, 3, 32, 32]).astype(np.float32))
mindspore.export(model, inputs, file_name="model", file_format="AIR")
