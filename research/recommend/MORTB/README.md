# [Contents](#contents)

- [MORTB](#mortb)

- [Dataset](#dataset)

- [Environment Requirements](#environment-requirements)

- [Script Description](#script-description)

- [Quick Start](#quick-start)

    - [Data Process](#data-process)
    - [Simulation Process](#simulation-process)

- [Performance Description](#performance-description)

- [Description of Random Situation](#description-of-random-situation)

# [MORTB](#contents)

This repo contains code for the experiments of the following paper:

- Weitong Ou, Bo Chen, Weiwen Liu, Xinyi Dai, Weinan Zhang, Wei Xia, Xuan Li, Ruiming Tang, and Yong Yu. 2023. Optimal Real-Time Bidding Strategy for Position Auctions in Online Advertising. In CIKM' 23.

In this work, we study the optimal bidding strategy for position auctions in Real-Time Bidding. The position auctions we study include both sponsored search and display advertising with multiple ad slots. In particular, we aim at maximizing the advertising value within the budget constraint for an advertiser with explicit modeling of the position effects.

# [Dataset](#contents)

[Criteo Attribution Bidding Dataset](https://ailab.criteo.com/criteo-attribution-modeling-bidding-dataset/) from the following paper:

- Diemert E, Meynet J, Galland P, et al. Attribution modeling increases efficiency of bidding in display advertising[M] // Proceedings of the ADKDD'17. 2017: 1-6.

# [Environment Requirements](#contents)

- Framework
    - [MindSpore](https://www.mindspore.cn/install/en)

- For more information, please check the resources below：
    - [MindSpore Tutorials](https://www.mindspore.cn/tutorials/en/master/index.html)
    - [MindSpore Python API](https://www.mindspore.cn/docs/en/master/api_python/mindspore.html)

# [Script Description](#contents)

```text
.
└─MORTB
  ├─README.md                         # descriptions
  ├─script
    ├─preprocess.sh                   # script for preprocessing the dataset
    ├─run_eval.sh                     # test for bidding
    └─run_search.sh                   # search the best parameter for bidding
  ├─src
    ├─click_models.py.py              # User click models.
    ├─ctr_cvr_prediction.py           # code for predicting CTR and CVR for the dataset
    └─landscape.py                    # code for predicting the auction landscape
  ├─data_prepraration.py              # Preprocessing raw data, including CTR/CVR training and list generation.
  └─main.py                           # Bidding simulation.
```

<!--
*src/click_models.py*: User click models borrowed from [Here](https://github.com/QingyaoAi/Unbiased-Learning-to-Rank-with-Unbiased-Propensity-Estimation/blob/master/Unbiased_LTR/click_models.py). -->

# [Quick Start](#contents)

After installing [MindSpore](https://www.mindspore.cn/install/en) via the official website and downloading the dataset , you can start as follows:

## [Data Process](#contents)

The following script includes preprocessing data for CTR and CVR training, training CTR and CVR model, and generating auction lists based on the prediction models.

Note that each of these steps will take a long time. It is recommended to run the commands in the script separately.

```shell
bash scripts/preprocess.sh RAW_DATA_PATH TEMP_MODEL_PATH DATA_PATH
```

*RAW_DATA_PATH* is the directory where the raw data file *criteo_attribution_dataset.tsv.gz* is stored.

*TEMP_MODEL_PATH* is the temporary directory to store the CTR and CVR model.

*DATA_PATH* is the directory to store the generating lists.

**Key Parameters for `data_preparation.py`**

| argument           | usage                                                        |
| ------------------ | ------------------------------------------------------------ |
| `--data_path`      | The path of the raw data file                                |
| `--mode`           | Choose from 'preprocess', 'train', 'test', or 'generate'     |
| `--model_path`     | Directory to store the train CTR and CVR model.              |
| ``--task``         | Choose from 'ctr' and 'cvr'. (Only use in 'train' or 'test' mode) |
| `--output_path`    | Directory for the generating lists. (Only use in 'generate' mode) |
| `--ctr_model_path` | Path to the best CTR model. (Only use in 'generate' mode)    |
| `--cvr_model_path` | Path to the best CVR model. (Only use in 'generate' mode)    |

## [Simulation Process](#contents)

The following two scripts are used to simulate the bidding process:

```shell
bash scripts/run_search.sh DATA_PATH RESULT_PATH
bash scripts/run_eval.sh DATA_PATH RESULT_PATH LAMBDA
```

*DATA_PATH* is the directory to store the generated lists. (same as before)

*RESULT_PATH* is the directory for the output results.

The differences of *run_search.sh* and *run_eval.sh* is that: the former one uses bisection search to find the best parameter in the bidding strategy, while the latter one uses a fixed *LAMBDA$ provided by the user. You can config the parameters in the script to customize the maximum list length, evaluating days, the campaign ID, the budget (the proportion to the maximum cost),  the bidding strategies, and the decay factor $\gamma$ .

**Key Parameters for `main.py`**

| argument      | usage                                       |
| ------------- | ------------------------------------------- |
| `--campaign`  | The campaign ID.                            |
| `--start_day` | Start day, from 1-29.                       |
| `--end_day`   | End day, from 2-30, larger than start_day. |
| `--length`    | The length of the list, less than 10.       |
| `--budget`    | The proportion of the original cost.        |
| `--gamma`     | The exponential factor for the decay.       |

# [Performance Description](#contents)

The AUC of the CTR and CVR models are 0.683 and 0.844 respectively. Based on the above models, the ROI and CPA of 10 campaigns are reported in the following:

**Average ROI over 30 days.**

| Campaign |   MKB  | Linear | S_ORTB | M_ORTB |
|:--------:|:------:|:------:|:------:|:------:|
| 10341182 | 4.2672 | 4.2746 | 4.4869 | 4.4999 |
| 30801593 | 2.9457 | 2.9549 | 3.0689 | 3.0880 |
| 17686799 | 2.9139 | 2.9863 | 3.1195 | 3.1334 |
| 15398570 | 3.9434 | 3.9938 | 4.2274 | 4.2376 |
|  5061834 | 2.5936 | 2.6344 | 2.7429 | 2.7580 |
| 15184511 | 4.7815 | 4.8539 | 5.2097 | 5.2140 |
| 29427842 | 3.9739 | 4.0066 | 4.2549 | 4.2681 |
| 28351001 | 3.2693 | 3.3173 | 3.5364 | 3.5493 |
| 18975823 | 4.2633 | 4.3191 | 4.6241 | 4.6447 |
| 31772643 | 2.8946 | 2.9221 | 3.0430 | 3.0584 |
| Avg. Improvements | 0.00%  | 1.16%  | 6.88%  | **7.27%** |

**Average CPA(1e-2) over 30 days.**

| Campaign |   MKB   |  Linear |  S_ORTB |  M_ORTB |
|:--------:|:-------:|:-------:|:-------:|:-------:|
| 10341182 |  1.4983 |  1.4956 |  1.3919 |  1.3811 |
| 30801593 |  1.6614 |  1.6563 |  1.5942 |  1.5854 |
| 17686799 |  7.6530 |  7.4359 |  7.1223 |  7.1036 |
| 15398570 |  4.0715 |  4.0133 |  3.7873 |  3.7803 |
|  5061834 | 11.0485 | 10.8765 | 10.4644 | 10.4247 |
| 15184511 |  3.7242 |  3.4924 |  2.5432 |  2.2780 |
| 29427842 |  4.8931 |  4.8518 |  4.5800 |  4.5749 |
| 28351001 |  2.4330 |  2.3868 |  2.1311 |  2.0796 |
| 18975823 |  3.5325 |  3.4832 |  3.2294 |  3.2094 |
| 31772643 |  3.0084 |  2.9801 |  2.8630 |  2.8499 |
| Avg. Improvements |  0.00%  |  1.96%  |  8.77%  | **9.78%** |

| Campaign |   MKB   |  Linear |  S_ORTB |  M_ORTB |
|:--------:|:-------:|:-------:|:-------:|:-------:|
| 10341182 | 16.5939 | 16.6107 | 17.5671 | 17.7524 |
| 30801593 |  1.7407 |  1.7455 |  1.8200 |  1.8462 |
| 17686799 |  6.3142 |  6.4474 |  6.7958 |  6.8851 |
| 15398570 | 15.9425 | 16.1392 | 17.2523 | 17.3463 |
|  5061834 |  2.5486 |  2.5904 |  2.7029 |  2.7337 |
| 15184511 |  9.1649 |  9.3374 | 10.1950 | 10.2104 |
| 29427842 | 14.8232 | 14.9615 | 15.8560 | 15.9525 |
| 28351001 |  2.0285 |  2.0601 |  2.2591 |  2.2969 |
| 18975823 | 12.4384 | 12.6075 | 13.6509 | 13.7669 |
| 31772643 |  3.3818 |  3.4060 |  3.5592 |  3.5998 |
| Avg. Improvements |  0.00%  |  1.09%  |  7.86%  | **8.72%** |

# [Description of Random Situation](#contents)

We set the random seed before data preprocessing in `data_prepraration.py`.