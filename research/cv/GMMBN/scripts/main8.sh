#!/bin/bash
export GLOG_v=4
export HCCL_CONNECT_TIMEOUT=6000
echo "=============================================================================================================="
echo "Please run the script as: "
echo "bash run.sh RANK_SIZE"
echo "For example: bash run.sh"
echo "=============================================================================================================="
export RANK_SIZE=8

for((i=1;i<$RANK_SIZE;i++))
do
    rm -rf device$i
    mkdir device$i
    cp -r ./src/* ./device$i
    cd ./device$i
    export DEVICE_ID=$i
    export RANK_ID=$i
    echo "start training for device $i"
    python ./main.py --data_config $1&
    cd ../
done
rm -rf device0
mkdir device0
cp -r ./src/* ./device0
cd ./device0
export DEVICE_ID=0
export RANK_ID=0
echo "start training for device 0"
python ./main.py --data_config $1
if [ $? -eq 0 ];then
    echo "training success"
else
    echo "training failed"
    exit 2
fi
cd ../
