# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

from mindspore import nn
from mindspore import ops

class MLP(nn.Cell):
    def __init__(self, name, params):
        super(MLP, self).__init__()
        self.model_name = name
        self.params = params
        self.dataset_source = params["dataset_source"]
        if params["dataset_source"] == "criteo":
            self.category_embeddings = nn.CellList([
                nn.Embedding(55824, 64),
                nn.Embedding(5443, 64),
                nn.Embedding(13073, 64),
                nn.Embedding(13170, 64),
                nn.Embedding(3145, 64),
                nn.Embedding(33843, 64),
                nn.Embedding(14304, 64),
                nn.Embedding(11, 64),
                nn.Embedding(13601, 64)
            ])

            self.numeric_embeddings = nn.CellList([
                nn.Embedding(64, 64),
                nn.Embedding(16, 64),
                nn.Embedding(128, 64),
                nn.Embedding(64, 64),
                nn.Embedding(128, 64),
                nn.Embedding(64, 64),
                nn.Embedding(512, 64),
                nn.Embedding(512, 64)
            ])
            presize = 1088

        if name == "MLP_FSIW":
            print("using elapse feature")
            presize += 1

        self.mlp = nn.CellList([
            nn.Dense(presize, 256, activation='leakyrelu'),
            nn.Dense(256, 256, activation='leakyrelu'),
            nn.Dense(256, 128, activation='leakyrelu')
        ])

        if self.model_name in ["MLP_SIG", "MLP_FSIW"]:
            self.mlp.append(nn.Dense(128, 1))
        else:
            raise ValueError("model name {} not exist".format(name))

    def construct(self, x):
        if self.dataset_source == "criteo":
            cate_embeddings = []
            nume_embeddings = []
            if self.model_name == "MLP_FSIW":
                for i in range(8):
                    nume_embeddings.append(self.numeric_embeddings[i](x[:, i].int()))

                for i in range(9):
                    cate_embeddings.append(self.category_embeddings[8 - i](x[:, -i - 2].int()))

                features = nume_embeddings + cate_embeddings + [x[:, -1:]]
                x = ops.Concat(axis=1)(features)
            else:
                for i in range(8):
                    nume_embeddings.append(self.numeric_embeddings[i](x[:, i].int()))

                for i in range(9):
                    cate_embeddings.append(self.category_embeddings[8 - i](x[:, -i - 2].int()))

                features = nume_embeddings + cate_embeddings
                x = ops.Concat(axis=1)(features)

        for layer in self.mlp:
            x = layer(x)

        if self.model_name in ["MLP_SIG", "MLP_FSIW"]:
            return x
        raise NotImplementedError()

def get_model(name, params):
    if name in ["MLP_tn_dp", "MLP_FSIW"]:
        return MLP(name, params)
    if name == "MLP_SIG":
        if params["base_model"] == "MLP":
            return MLP(name, params)
    else:
        raise NotImplementedError()
    return 0
