import operator
from itertools import islice, repeat
import collections
from collections import OrderedDict
import numpy as np

import mindspore
import mindspore.nn as nn
import mindspore.ops as ops
from mindspore import Parameter

def _ntuple(n):
    def parse(x):
        if isinstance(x, collections.abc.Iterable):
            return tuple(x)
        return tuple(repeat(x, n))
    return parse
_pair = _ntuple(2)

class TwoConv2d(nn.Cell):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, groups=1, bias=True):
        kernel_size = _pair(kernel_size)
        stride = _pair(stride)
        padding = _pair(padding)
        dilation = _pair(dilation)
        super(TwoConv2d, self).__init__(
            in_channels, out_channels, kernel_size, stride, padding, dilation,
            False, _pair(0), groups, bias, padding_mode='zeros')

    def construct(self, inputs, idx=0):
        # return ops.conv2d(input, self.weight, self.bias, self.stride, self.padding, self.dilation, self.groups), idx
        return ops.conv2d(inputs, self.weight, pad_mode="pad", \
                          padding=self.padding, stride=self.stride, dilation=self.dilation, group=self.groups), idx

def batch_norm(X, gamma, beta, w, moving_mean, moving_var, is_training=True, eps=1e-5, momentum=0.9):
    ## w.shape --> B * 1 * H * W
    B, _, H, W = X.shape
    w = w / w.sum() # probability for each sample
    if len(w.shape) != 4:
        w = w.view(B, 1, H, W)
    shape_2d = (1, X.shape[1], 1, 1)

    mu = ops.ReduceMean()(w * X, dim=(0, 2, 3)).view(shape_2d)
    var = ops.ReduceMean()(w * (X - mu) ** 2, dim=(0, 2, 3)).view(shape_2d) # biased
    #X_hat = (X - mu) / torch.sqrt(var + eps)

    if is_training:
        X = (X - mu) / ops.Sqrt()(var + eps)
        moving_mean = moving_mean.view(shape_2d)
        moving_var = moving_var.view(shape_2d)

        moving_mean = momentum * moving_mean + (1.0 - momentum) * mu
        moving_var = momentum * moving_var + (1.0 - momentum) * var
    else:
        moving_mean = moving_mean.view(shape_2d)
        moving_var = moving_var.view(shape_2d)

        X = (X - moving_mean) / ops.Sqrt()(moving_var + eps)

    out = gamma.view(shape_2d) * X + beta.view(shape_2d)
    out = out * w
    del shape_2d
    return out, moving_mean.squeeze(), moving_var.squeeze()

class _GMMNormBase(nn.Cell):
    _version = 2
    _author = 'chenglc'
    _constants__ = ["track_running_stats", "momentum", "eps", "num_features", "affine"]
    num_features: int
    num_mixtures: int
    eps: float
    momentum: float
    affine: bool
    track_running_stats: bool
    def __init__(
            self,
            num_features: int,
            num_mixtures: int = 4,
            eps: float = 1e-5,
            momentum: float = 0.1,
            affine: bool = True,
            track_running_stats: bool = True,
            dtype=mindspore.float32
            ) -> None:

        super(_GMMNormBase, self).__init__()
        self.num_features = num_features
        self.num_mixtures = num_mixtures
        self.eps = eps
        self.momentum = momentum
        self.affine = affine
        self.track_running_stats = track_running_stats

        if self.affine:
            self.weight = mindspore.ParameterTuple(\
                [Parameter(ops.ones(num_features, dtype), name=str(_)) for _ in range(self.num_mixtures)])
            self.bias = mindspore.ParameterTuple(\
                [Parameter(ops.Zeros()(num_features, dtype), name=str(_)+'_') for _ in range(self.num_mixtures)])
        else:
            #import pdb
            #pdb.set_trace()
            self.register_parameter("weight", None)
            self.register_parameter("bias", None)
        if self.track_running_stats:
            self.running_mean = Parameter(ops.Zeros()((num_mixtures, num_features), dtype), requires_grad=False)
            self.running_var = Parameter(ops.Ones()((num_mixtures, num_features), dtype), requires_grad=False)
            self.running_taus = Parameter(ops.Zeros()((num_mixtures, 1), dtype), requires_grad=False)# /num_mixtures
            self.num_batches_tracked = Parameter(mindspore.Tensor(np.array([0]), dtype=mindspore.float32), \
                                                 requires_grad=False)
            # self.running_mean = ops.Zeros()((num_mixtures,num_features), dtype)
            # self.running_var = ops.Ones()((num_mixtures,num_features), dtype)
            # self.running_taus = ops.Zeros()((num_mixtures,1),dtype)
            # self.num_batches_tracked = mindspore.Tensor(0, dtype=mindspore.float32)
        else:
            self.register_buffer("running_mean", None)
            self.register_buffer("running_var", None)
            self.register_buffer("running_taus", None)
            self.register_buffer("num_batches_tracked", None)
        self.reset_parameters()
    def reset_running_stats(self) -> None:

        if self.track_running_stats:
            self.running_mean = ops.ZerosLike()(self.running_mean)  # type: ignore[union-attr]
            self.running_var = ops.fill(type=self.running_var.dtype, shape=self.running_var.shape, value=1)  # type: ignore[union-attr]
            self.running_taus = ops.fill(type=self.running_taus.dtype, shape=self.running_taus.shape, value=1.0/self.num_mixtures) # type: ignore[union-attr]
            self.num_batches_tracked = ops.ZerosLike()(self.num_batches_tracked) # type: ignore[union-attr,operator]
    def reset_parameters(self) -> None:
        self.reset_running_stats()
        # if self.affine:
        #     for idx in range(self.num_mixtures):
        #         self.weight[idx] = initializer(One(), self.weight[idx].shape)
        #         self.bias[idx] = initializer(Zero(), self.bias[idx].shape)
        #     self.weight = mindspore.ParameterTuple(tuple(self.weight))
        #     self.bias = mindspore.ParameterTuple(tuple(self.bias))
    def _check_input_dim(self, inputs):
        raise NotImplementedError

    def extra_repr(self):
        return ("{num_features}, eps={eps}, momentum={momentum}, affine={affine}, "
                "track_running_stats={track_running_stats}".format(**self.__dict__)
                )

    def _load_from_state_dict(self, state_dict, prefix, local_metadata, strict, \
                              missing_keys, unexpected_keys, error_msgs):

        version = local_metadata.get("version", None)
        if (version is None or version < 2) and self.track_running_stats:
            num_batches_tracked_key = prefix + "num_batches_tracked"
        if num_batches_tracked_key not in state_dict:
            state_dict[num_batches_tracked_key] = mindspore.Tensor(0, dtype=mindspore.float16)

        super(_GMMNormBase, self)._load_from_state_dict(state_dict, prefix, local_metadata, \
                                                        strict, missing_keys, unexpected_keys, error_msgs)

class MyBatchNorm2d(_GMMNormBase):
    def __init__(self,
            num_features,
            num_mixtures=4,
            eps=1e-8,
            momentum=0.1,
            affine=True,
            track_running_stats=True,
            dtype=mindspore.float32):
        factory_kwargs = {'dtype': dtype}
        super(MyBatchNorm2d, self).__init__(num_features, num_mixtures, \
                                            eps, momentum, affine, track_running_stats, **factory_kwargs)

    def gaussian_probability(self, x):
        #B, C, H, W = x.shape
        P = []
        for idx in range(self.num_mixtures):
            tmp = (x - self.running_mean[idx][None, :, None, None]) / (ops.Sqrt()\
                (self.running_var[idx][None, :, None, None] + self.eps))
            tmp = ops.clip_by_value(ops.exp(-0.5 * tmp**2).mean(axis=1).view(-1), self.eps, 1)
            P.append(ops.stop_gradient(tmp))
        P = ops.stack(P, axis=0)
        #P --> k * (BHW)
        return P

    def update_estep(self, p):
        #p.shape --> k * (BHW)
        w = p * self.running_taus
        w = w / w.sum(axis=0, keepdims=True)#sum over k is 1 for each column
        return ops.stop_gradient(w)

    def update_taus(self, wt, momentum):
        # w: k * (BHW)
        wt = wt.mean(axis=1)
        wt = wt / wt.sum()
        wt = wt.view(self.running_taus.shape)
        wt = ops.stop_gradient(wt)
        self.running_taus = self.running_taus * (1 - momentum) + wt * momentum
        self.running_taus = self.running_taus / self.running_taus.sum()

    def get_momentum(self):
        if self.momentum is None:
            exponential_average_factor = 0.0
        else:
            exponential_average_factor = self.momentum
        if self.training and self.track_running_stats:
            if self.num_batches_tracked is not None:
                self.num_batches_tracked += 1
                if self.momentum is None:  # use cumulative moving average
                    exponential_average_factor = 1.0 / float(self.num_batches_tracked)
                else:  # use exponential moving average
                    exponential_average_factor = self.momentum
        return exponential_average_factor

    def split_bn(self, x):
        out = []
        momentum = self.get_momentum()
        # momentum = ops.stop_gradient(momentum)
        split_size = x.shape[0] // self.num_mixtures \
            if x.shape[0] % self.num_mixtures == 0 else x.shape[0] // self.num_mixtures + 1 # 64 to 32
        if split_size != 128:
            print(x.shape, split_size)
        x = x.split(output_num=x.shape[0] // split_size) # 注意pytorch的tensor.split中split_size是切分后每个张量的size，而mindspore是切分为split_size个张量

        # assert len(x) == self.num_mixtures
        for idx in range(self.num_mixtures):
            x_tmp = x[idx]
            mean = x_tmp.mean([0, 2, 3])
            var = ops.ReduceMean()((x_tmp - mean[None, :, None, None]) ** 2, (0, 2, 3))
            n = ops.size(x_tmp) / x_tmp.shape[1]

            if self.training:  # aux BN only relevant while training
                self.running_mean[idx] = momentum * mean + (1-momentum) * self.running_mean[idx]
                self.running_mean[idx] = ops.stop_gradient(self.running_mean[idx])
                self.running_var[idx] = momentum * var * n / (n - 1) + (1 - momentum) * self.running_var[idx]
                x_tmp = (x_tmp - mean[None, :, None, None]) / (ops.Sqrt()(var[None, :, None, None]) +  self.eps)
            else:
                x_tmp = (x_tmp - self.running_mean[idx][None, :, None, None]) / \
                    (ops.Sqrt()(self.running_var[idx][None, :, None, None]) + self.eps)

            x_tmp = x_tmp * self.weight[idx][None, :, None, None] + self.bias[idx][None, :, None, None]
            out.append(x_tmp)

        return ops.Concat(axis=0)(out)

    def _check_input_dim(self, inputs):
        if input.dim() != 4:
            raise ValueError('expected 4D input (got {}D input)'
                             .format(inputs.dim()))

    def construct(self, inputs: mindspore.Tensor, split_flag: int = 1):
        if split_flag == 0:
            return self.split_bn(inputs), split_flag

        momentum = self.get_momentum()
        probability = self.gaussian_probability(inputs)
        w = self.update_estep(probability)

        if self.training:
            self.update_taus(w, momentum)

        B, C, H, W = inputs.shape

        inputs = inputs.transpose((1, 0, 2, 3)).view(C, -1)
        out = None

        for idx in range(self.num_mixtures):
            wx = w[idx]

            mu = ops.ReduceSum()(wx.view(1, -1) * inputs, axis=1) / wx.sum()
            var = ops.ReduceSum()(wx.view(1, -1) * (inputs - mu.view(-1, 1))**2, axis=1) / wx.sum()

            if self.training:
                self.running_mean[idx] = (1 - momentum) * self.running_mean[idx] + momentum * mu
                self.running_var[idx] = (1 - momentum) * self.running_var[idx] + momentum * var
                x_std = (inputs - mu.view(-1, 1)) / ops.Sqrt()(var.view(-1, 1) + self.eps)
            else:
                x_std = (inputs - self.running_mean[idx].view(-1, 1)) / \
                    ops.Sqrt()(self.running_var[idx].view(-1, 1) + self.eps)
            if out is None:
                out = wx.view(1, -1) * (self.weight[idx].view(-1, 1) * x_std + self.bias[idx].view(-1, 1))
            else:
                out = out + wx.view(1, -1) * (self.weight[idx].view(-1, 1) * x_std + self.bias[idx].view(-1, 1))

        out = out.view(C, B, H, W).transpose((1, 0, 2, 3))
        return out, split_flag

class GMMBatchNorm(nn.Cell):
    _version = 1
    def __init__(self, num_features, eps=1e-8, momentum=0.1, sync=False,\
                  affine=True, track_running_stats=True, num_class=10, k=10):
        super(GMMBatchNorm, self).__init__()

        #self.bns = nn.ModuleList([GMMLayerNorm2d(dim=num_features,eps=eps) for _ in range(k) ])
        if sync:
            self.bns = nn.ModuleList([nn.SyncBatchNorm(num_features, eps, momentum, \
                                                       affine, track_running_stats) for _ in range(k)])
        else:
            self.bns = nn.ModuleList([nn.BatchNorm2d(num_features, eps, momentum, \
                                                     affine, track_running_stats) for _ in range(k)])
        self.k = k
        self.num_class = num_class
        self.taus = Parameter((ops.ones((k, 1), mindspore.float16)) / k)
        self.taus.requires_grad = False
        self.tau_factor = 0.1
        self.eps = eps

    def E_step(self, P):
        #P.shape --> k * (BHW)
        W = P * self.taus
        W = W / W.sum(dim=0, keepdim=True)
        return W

    def update(self, Wx):
        W = Wx.clone()
        # W.shape --> k * (BHW)
        W = W.mean(dim=1)
        #W = W.sum(dim=1)
        #W = W / W.sum()
        W = W.view(self.taus.shape)
        self.taus = self.taus * (1 - self.tau_factor) + W * self.tau_factor
        self.taus = self.taus/ self.taus.sum()

    def gaussian_p(self, x):
        P = []
        for idx in range(len(self.bns)):
            tmp = (x - self.bns[idx].running_mean[None, :, None, None]) /\
                  (ops.Sqrt()(self.bns[idx].running_var[None, :, None, None] + self.eps))
            tmp = ops.clip_by_value(ops.exp(-0.5 * tmp**2).mean(dim=1).view(-1), 0, 1)
            P.append(tmp)
        P = ops.stack(P, axis=0)
        return P

    def step(self, x, W):
        out = None
        for w, bn in zip(W, self.bns):
            w = w / w.sum()
            w = w.view(x.shape[0], 1, x.shape[2], x.shape[3])
            if out is None:
                out = bn(w * x)
            else:
                out = out + bn(w * x)
        return out

    def compute_gmm(self, x):
        P = self.gaussian_p(x)
        W = self.E_step(P)
        if self.training:
            self.update(W)
        out = self.step(x, W)
        return out

    def split_bn(self, x):
        if self.training:  # aux BN only relevant while training
            split_size = x.shape[0] // self.k \
                if x.shape[0] % self.k == 0 else x.shape[0] // (self.k - 1)# 64 to 32
            split_input = x.split(split_size)
            out = []
            for xt, bn in zip(split_input, self.bns):
                out.append(bn(xt))
            return ops.Concat(axis=0)(out)
        return self.bns[0](x)

    def reset_running_stats(self):
        for bn in self.bns:
            bn.reset_running_stats()

    def reset_parameters(self):
        for bn in self.bns:
            bn.reset_parameters()

    def _check_input_dim(self, inputs):
        if input.dim() != 4:
            raise ValueError('expected 4D input (got {}D input)'
                             .format(inputs.dim()))

    def construct(self, x, idx=0): #idx 0 --> weak bn; idx 1 --> strong bn
        self._check_input_dim(x)

        if idx == 0:
            out = self.split_bn(x)
        elif idx == 1:
            out = self.compute_gmm(x)
        else:
            raise ValueError("idx error")
        return out, idx


class TwoInputSequential(nn.Cell):
    r"""A sequential container forward with two inputs.
    """

    def __init__(self, *args):
        super(TwoInputSequential, self).__init__()
        if len(args) == 1 and isinstance(args[0], OrderedDict):
            for key, module in args[0].items():
                self.add_module(key, module)
        else:
            for idx, module in enumerate(args):
                self.add_module(str(idx), module)

    def _get_item_by_idx(self, iterator, idx):
        """Get the idx-th item of the iterator"""
        size = len(self)
        idx = operator.index(idx)
        if not -size <= idx < size:
            raise IndexError('index {} is out of range'.format(idx))
        idx %= size
        return next(islice(iterator, idx, None))

    def __getitem__(self, idx):
        if isinstance(idx, slice):
            return TwoInputSequential(OrderedDict(list(self._modules.items())[idx]))
        return self._get_item_by_idx(self._modules.values(), idx)

    def __setitem__(self, idx, module):
        key = self._get_item_by_idx(self._modules.keys(), idx)
        return setattr(self, key, module)

    def __delitem__(self, idx):
        if isinstance(idx, slice):
            for key in list(self._modules.keys())[idx]:
                delattr(self, key)
        else:
            key = self._get_item_by_idx(self._modules.keys(), idx)
            delattr(self, key)

    def __len__(self):
        return len(self._modules)

    def __dir__(self):
        keys = super(TwoInputSequential, self).__dir__()
        keys = [key for key in keys if not key.isdigit()]
