# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import os
import json
import random
import inspect
import argparse
import numpy as np
from mindspore import set_seed

def open_json(path_):
    with open(path_) as fh:
        data = json.load(fh)
    return data

def dump_json(path_, data):
    with open(path_, 'w') as fh:
        json.dump(data, fh, indent=2)
    return data

def str2bool(text=""):
    text = text.lower()
    var = bool(text)
    return var

def check_path(file_path):
    if not os.path.exists(file_path):
        os.makedirs(file_path)

def arg_parser():
    return argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

def get_objects(name_space):
    res = {}
    for name, obj in inspect.getmembers(name_space):
        if inspect.isclass(obj):
            res[name] = obj
    return res

def set_global_seeds(i):
    np.random.seed(i)
    random.seed(i)
    set_seed(i)
