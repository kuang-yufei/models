# Rethinking Branching on Exact Combinatorial Optimization Solver: The First Deep Symbolic Discovery Framework

This is the code of paper **Rethinking Branching on Exact Combinatorial Optimization Solver: The First Deep Symbolic Discovery Framework**. [[paper]]