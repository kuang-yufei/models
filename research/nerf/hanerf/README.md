
# Contents

* [Contents](#contents)
    * [HA-NeRF Description](#nerf-description)
        * [Model Architecture](#model-architecture)
        * [Dataset](#dataset)
    * [Environment Requirements](#environment-requirements)
    * [Quick Start](#quick-start)
        * [Prepare the dataset](#prepare-the-dataset)
        * [Prepare the model](#prepare-the-model)
        * [Run the scripts](#run-the-scripts)
    * [Script Description](#script-description)
        * [Script and Sample Code](#script-and-sample-code)
        * [Script Parameters](#script-parameters)
    * [Training](#training)
        * [Training Process](#training-process)
    * [Evaluation](#evaluation)
        * [Evaluation Process](#evaluation-process)
            * [Evaluation on GPU](#evaluation-on-gpu)
        * [Evaluation result](#evaluation-result)
    * [Inference](#inference)
        * [Inference Process](#inference-process)
            * [Inference on GPU](#inference-on-gpu)
        * [Inference result](#inference-result)
    * [Video exporting](#video-exporting)
        * [Video exporting Process](#video-exporting-process)
            * [Video exporting on GPU](#video-exporting-on-gpu)
        * [Video exporting result](#video-exporting-result)
    * [3D mesh exporting](#3d-mesh-exporting)
        * [3D mesh exporting Process](#3d-mesh-exporting-process)
            * [3D mesh exporting on GPU](#3d-mesh-exporting-on-gpu)
        * [3D mesh exporting result](#3d-mesh-exporting-result)
   * [Model Description](#model-description)
        * [Performance](#performance)
   * [Description of Random Situation](#description-of-random-situation)
   * [ModelZoo Homepage](#modelzoo-homepage)

## [HA-NeRF Description](#contents)

NeRF is a method that achieves state-of-the-art results for synthesizing novel
views of complex scenes by optimizing an underlying continuous volumetric scene function
using a sparse set of input views. The model synthesizes views by
querying 5D coordinates along camera rays and uses classic volume rendering techniques to project
the output colors and densities into an image. Because volume rendering is naturally differentiable,
the only input required to optimize our representation is a set of images with known camera poses.

Some useful notes:

* NeRF consists of the coarse and fine models with the same architecture.
* Positional Encoder is used for higher dimensional space for each input coordinate.
* Hierarchical sampler allocates more samples to regions we expect to contain visible content.

hallucinated NeRF (Ha-NeRF) framework that can hallucinate the realistic radiance field from unconstrained tourist images with
variable appearances and occluders.
For appearance hallucination, we propose a CNN-based appearance encoder and a view-consistent appearance loss
to transfer consistent photometric appearance in different
views. This design gives our method the flexibility to transfer the appearance of unlearned images. For anti-occlusion,
we utilize an MLP to learn an image-dependent 2D visibility mask with an anti-occlusion loss that can automatically
separate the static components with high accuracy during
training. Experiments on several landmarks confirm the superior of the proposed method in terms of appearance hallucination and anti-occlusion.
Our contributions can be summarized as follows:

1. The Ha-NeRF is proposed to recover the appearance hallucination radiance fields from a group of images with variable appearances and occluders.
2. An appearance hallucination module is developed to transfer the view-consistent appearance to novel views.
3. An anti-occlusion module is modeled imagedependently to perceive the ray visibility.

NeRF paper:

[Paper](https://arxiv.org/abs/2003.08934): Ben Mildenhall, Pratul P. Srinivasan, Matthew Tancik,
Jonathan T. Barron, Ravi Ramamoorthi, Ren Ng. CVPR, 2020.

HA-NeRF paper:

[Paper](https://arxiv.org/abs/2111.15246): Xingyu Chen, Qi Zhang, Xiaoyu Li, Yue Chen, Ying Feng, Xuan Wang, Jue Wang. CVPR, 2022.

### [Model Architecture](#contents)

NeRF represents a scene using a fully-connected (non-convolutional) deep network,
whose input is a single continuous 5D coordinate (spatial location (x,y,z) and viewing direction (θ,ϕ))
and whose output is the volume density and view-dependent emitted radiance at
that spatial location. Model encourages the representation to be multiview consistent by restricting
the network to predict the volume density as a function of only the location
x, while allowing the RGB color to be predicted as a function of both location
and viewing direction. To accomplish this, the multilayer perceptron first processes the input
3D coordinate x with 8 fully-connected layers (using ReLU activations and 256
channels per layer), and outputs the volume density and a 256-dimensional feature vector. This
feature vector is then concatenated with the camera ray’s viewing direction and
passed to one additional fully-connected layer (using a ReLU activation and 128
channels) that outputs the view-dependent RGB color.

As for HA-NeRF, they get an image, use a CNN to encode it into an appearance latent vector.
They synthesize images by sampling location x and viewing direction d of camera rays,
feeding them with into MLPs to produce a color c and volume density σ and rendering a reconstructed
input image. Given an image-dependent transient embedding they use an MLP to map pixel location p to a visible
possibility, so that they can disentangle static and transient phenomena of the images with an occlusion loss.

### [Dataset](#contents)

* Phototourism: real images of complex scenes of landmarks. Dataset contain images, poses, depth maps, and co-visibility estimates.

## [Environment Requirements](#contents)

* Download the datasets and locate to some folder `/path/to/the/dataset`:

    * [phototourism](https://www.cs.ubc.ca/~kmyi/imw2020/data.html)

* Install the requirements:

Use requirement.txt file with the following command:

```bash
pip install -r requirements.txt
```

Or you can install the following packages manually:

```text
mindspore-dev==2.0.0.dev20230609
Pillow
numpy
tqdm
pandas
PyMCubes==0.1.2
trimesh==3.20.2
imageio[ffmpeg]
```

## [Quick Start](#contents)

### [Prepare the dataset](#contents)

(Optional but highly recommended) Run `python prepare_phototourism.py --root_dir $ROOT_DIR --img_downscale` {an integer, e.g. 2 means half the image sizes, value of 1 will consume large CPU memory about 40G for brandenburg gate} to prepare the training data and save to disk first, if you want to run multiple experiments or run on multiple gpus. This will largely reduce the data preparation step before training. If you want use this data preparation, set in `src/configs/phototourism_ds_config.json` `use_cache=True`. If set `use_cache=True`, but this cache doesn't exist, error will occur.

### [Prepare the model](#contents)

All necessary configs examples can be found in the project directory `src/configs`.

Training stage:

* dataset-based settings config (`phototourism_ds_config.json`)
* dataset-based train config (`phototourism_train_config_*.json`)
* nerf architecture config (`nerf_config.json`, the default in use)

Inference stage:

* dataset-based scene config (`inference/phototourism_scene_config_*.json`)
* dataset-based poses config (`inference/phototourism_init_pose_*.json`)
* dataset-based settings config (`phototourism_ds_config.json`)
* dataset-based train config (`phototourism_train_config_*.json`)
* nerf architecture config (`nerf_config.json`, the default in use)

Evaluation stage:

* dataset-based settings config (`phototourism_ds_config.json`)
* dataset-based train config (`phototourism_train_config_*.json`)
* nerf architecture config (`nerf_config.json`, the default in use, for eval use perturb=0.0, noise_std=0.0, n_samples=256, n_importance=256)

Note: \* - dataset scene name (one from brandenburg gate or trevi fountain).

1. Prepare the model directory: copy necessary configs for choosing dataset to some directory `/path/to/model_cfgs/` for future scripts launching.
2. Hyper parameters that recommended to be changed for training stage in dataset-based train config:

    * `batch_size`
    * `chunk`
    * `epochs`
    * `use_mask`
    * `lr`
    * `min_lr`

3. Download corresponding checkpoints if need.

### [Run the scripts](#contents)

After preparing directory with configs `/path/to/model_cfgs/` you can start training and evaluation:

* running on GPU

```shell
# distributed training on GPU
bash scripts/run_distribute_train_gpu.sh [TRAIN_CONFIG] [DS_CONFIG] [DATASET_PATH] [OUT_DIR] [DEVICE_NUM] [PRETRAINED_CKPT_PATH](optional)

# standalone training on GPU
bash scripts/run_standalone_train_gpu.sh [TRAIN_CONFIG] [DS_CONFIG] [DATASET_PATH] [OUT_DIR] [PRETRAINED_CKPT_PATH](optional)

# run eval on GPU
bash scripts/run_eval_gpu.sh [DATA_PATH] [DATA_CONFIG] [TRAIN_CONFIG] [CHECKPOINT_PATH] [OUT_PATH]

# run inference on GPU
bash scripts/run_infer_gpu.sh [SCENE_NAME] [POSE] [SCENE_CONFIG] [TRAIN_CONFIG] [DS_CONFIG] [HALLUCINATE_IMAGE_PATH] [CHECKPOINT_PATH] [OUT_PATH]

# run export 3d mesh
bash scripts/run_export_3d.sh [TRAIN_CONFIG] [CHECKPOINT_PATH] [OUT_STL]

# run export video
bash scripts/run_infer_gpu.sh [SCENE_NAME] [POSE] [SCENE_CONFIG] [TRAIN_CONFIG] [DS_CONFIG] [HALLUCINATE_IMAGE_PATH] [CHECKPOINT_PATH] [OUT_PATH]
```

## [Script Description](#contents)

### [Script and Sample Code](#contents)

```shell
├── hanerf
│   ├── convert_weights.py
│   ├── eval.py                                ## evaluation script
│   ├── export_3d_mesh.py                      ## 3d model exporting
│   ├── infer.py                               ## inference script
│   ├── prepare_phototourism.py                ## script for saving data cache
│   ├── requirements.txt                       ## requirements
│   ├── scripts
│   │   ├── run_distributed_training_gpu.sh    ## bash script for distributed training
│   │   ├── run_eval_gpu.sh                    ## bash script for evaluation
│   │   ├── run_export_3d.sh                   ## bash script for 3d mesh exporting
│   │   ├── run_export_video.sh                ## bash script for video exporting
│   │   ├── run_infer_gpu.sh                   ## bash script for inference
│   │   └── run_standalone_train_gpu.sh        ## bash script for single gpu training
│   ├── src
│   │   ├── __init__.py                        ## init file
│   │   ├── configs
│   │   │   ├── inference
│   │   │   │   ├── phototourism_init_pose_brandenburg_gate.json     ## phototourism dataset init pose config
│   │   │   │   ├── phototourism_init_pose_trevi_fountain.json       ## phototourism dataset init pose config
│   │   │   │   ├── phototourism_scene_config_brandenburg_gate.json  ## phototourism dataset scene config
│   │   │   │   └── phototourism_scene_config_trevi_fountain.json    ## phototourism dataset scene config
│   │   │   ├── nerf_config.json.                   ## NeRF architecture config
│   │   │   ├── phototourism_ds_config.json         ## phototourism dataset settings config
│   │   │   └── phototourism_train_config_bg.json   ## phototourism dataset train config for brandenburg gate
│   │   │   └── phototourism_train_config_tf.json   ## phototourism dataset train config for trevi fountain
│   │   ├── data
│   │   │   ├── __init__.py                      ## init file
│   │   │   ├── colmap_utils.py                  ## contain funcs for read binary prepared files
│   │   │   └── phototourism_mask_grid_sample.py ## mindspore based datasets
│   │   ├── models
│   │   │   ├── __init__.py                      ## init file
│   │   │   ├── nerf_system.py                   ## script from which the hanerf construction begins
│   │   │   └── networks.py                      ## contain encode appearance and implicit mask
│   │   ├── tools
│   │   │   ├── __init__.py                      ## init file
│   │   │   ├── callbacks.py                     ## custom callbacks
│   │   │   ├── common.py                        ## auxiliary funcs
│   │   │   ├── criterion.py                     ## hanerf loss functions
│   │   │   ├── metrics.py                       ## script for measure PSNR
│   │   │   ├── mlflow_funcs.py                  ## mlflow auxiliary funcs
│   │   │   └── rays.py                          ## rays sampling
│   │   ├── trainer
│   │   │   ├── __init__.py                      ## init file
│   │   │   ├── eval_one_step.py                 ## eval wrapper
│   │   │   └── train_one_step.py                ## training wrapper
│   │   └── volume_rendering
│   │       ├── __init__.py                      ## init file
│   │       ├── coordinates_samplers.py          ## Ha-NeRF coordinates sampling
│   │       ├── scene_representation.py          ## Ha-NeRF scene representation
│   │       └── volume_rendering.py              ## Ha-NeRF volume rendering pipeline
│   └── train.py                                 ## training script
```

### [Script Parameters](#contents)

Train config parameters (brandenburg gate):

```bash
{
  "n_vocab": 1500,        ## number of images in the dataset for nn.Embedding, should be set to an integer larger than the number of images (dependent on different scenes)
  "encode_a": true,       ## whether to encode appearance
  "n_a": 48,              ## number of embeddings for appearance
  "use_mask": false,      ## whether to use mask
  "encode_random": true,  ## whether to encode_random

  "maskrs_max": 5e-2,     ## regularize mask max value
  "maskrs_min": 6e-3,     ## regularize mask min value
  "maskrs_k": 1e-3,       ## regularize mask size
  "maskrd": 0.0,          ## regularize mask digit
  "weightKL": 1e-5,       ## regularize encA
  "weightRecA": 1e-3,     ## Rec A
  "weightMS": 1e-6,       ## mode seeking
  "min_scale": 0.5,       ## min scale

  "lr": 5e-4,             ## learning rate
  "min_lr": 1e-7,         ## min learning rate
  "epochs": 20,           ## number of training epochs

  "batch_size": 1024,     ## batch size
  "chunk": 16384          ## chunk size to split the input to avoid OOM
}
```

Dataset settings config parameters differ based on the dataset. But the common
dataset settings:

```bash
{
  "img_downscale": 2,     ## how much to downscale the images for phototourism dataset
  "use_cache": false      ## whether to use ray cache (make sure img_downscale is the same)
}
```

The following parameters must be changed for eval in nerf_config.json:

```bash
{
  "coarse_net_depth": 8,
  "coarse_net_width": 256,
  "fine_net_depth": 8,
  "fine_net_width": 256,

  "n_emb_xyz": 15,
  "n_emb_dir": 4,
  "n_samples": 64,          ## set 256 for eval
  "n_importance": 64,       ## set 256 for eval
  "use_disp": false,
  "perturb": 1.0,           ## set 0.0 for eval
  "noise_std": 1.0          ## set 0.0 for eval
}
```

## [Training](#contents)

To train the model, run `train.py`.

### [Training process](#contents)

Standalone training mode:

```bash
bash scripts/run_standalone_train_gpu.sh [TRAIN_CONFIG] [DS_CONFIG] [DATASET_PATH] [OUT_DIR] [PRETRAINED_CKPT_PATH](optional)
```

Bash script parameters:

* `TRAIN_CONFIG`: training loading settings config.
* `DS_CONFIG`: dataset scene loading settings config.
* `DATASET_PATH`: the path to the dataset scene.
* `OUT_DIR`: output directory to store the training meta information.
* `PRETRAINED_CKPT_PATH`: the path of pretrained checkpoint file, it is better to use absolute path.

### [Distribute training](#contents)

Distribute training mode:

```bash
bash scripts/run_distribute_train_gpu.sh [TRAIN_CONFIG] [DS_CONFIG] [DATASET_PATH] [OUT_DIR] [DEVICE_NUM] [PRETRAINED_CKPT_PATH](optional)
```

Script parameters:

* `TRAIN_CONFIG`: training loading settings config.
* `DS_CONFIG`: dataset scene loading settings config.
* `DATASET_PATH`: the path to the dataset scene.
* `OUT_DIR`: output directory to store the training meta information.
* `DEVICE_NUM`: devices count.
* `PRETRAINED_CKPT_PATH`: the path of pretrained checkpoint file, it is better
 to use absolute path.

Training result and checkpoints will be stored in the current path, whose folder name is "LOG".

## [Evaluation](#contents)

### [Evaluation process](#contents)

#### [Evaluation on GPU](#contents)

```bash
bash scripts/run_eval_gpu.sh [DATA_PATH] [DATA_CONFIG] [TRAIN_CONFIG] [CHECKPOINT_PATH] [OUT_PATH]
```

Script parameters:

* `DATA_PATH`: the path to the dataset scene.
* `DATA_CONFIG`: dataset scene loading settings config.
* `TRAIN_CONFIG`: training loading settings config.
* `CHECKPOINT_PATH`: the path of pretrained checkpoint file.
* `OUT_PATH`: output directory to store the evaluation result.

### [Evaluation result](#contents)

Result:

* Store predict images.
* Store the CSV file with image-wise PSNR value and the total PSNR.

## [Inference](#contents)

### [Inference process](#contents)

#### [Inference on GPU](#contents)

```bash
bash scripts/run_infer_gpu.sh [SCENE_NAME] [POSE] [SCENE_CONFIG] [TRAIN_CONFIG] [DS_CONFIG] [HALLUCINATE_IMAGE_PATH] [CHECKPOINT_PATH] [OUT_PATH]
```

Script parameters:

* `SCENE_NAME`: scene name for generate poses.
* `POSE`: init pose for NeRF-based inference rendering.
* `SCENE_CONFIG`：scene config file. Can be found in the training output directory.
* `TRAIN_CONFIG`: training loading settings config.
* `DS_CONFIG`: dataset scene loading settings config.
* `HALLUCINATE_IMAGE_PATH`: image path to apply to hallucinate scene.
* `CHECKPOINT_PATH`: path to checkpoint.
* `OUT_PATH`: path to the output directory with NeRF based rendered images for poses from config.

### [Inference result](#contents)

Predictions are the rendered images for passed poses in pose config stored in the `OUT_PATH` directory.

## [Video exporting](#contents)

### [Video exporting process](#contents)

#### [Video exporting on GPU](#contents)

```bash
bash scripts/run_infer_gpu.sh [SCENE_NAME] [POSE] [SCENE_CONFIG] [TRAIN_CONFIG] [DS_CONFIG] [HALLUCINATE_IMAGE_PATH] [CHECKPOINT_PATH] [OUT_PATH]
```

Script parameters:

* `SCENE_NAME`: scene name for generate poses.
* `POSE`: init for NeRF-based inference rendering and video exporting.
* `SCENE_CONFIG`：scene config file. Can be found in the training output directory.
* `TRAIN_CONFIG`: training loading settings config.
* `DS_CONFIG`: dataset scene loading settings config.
* `HALLUCINATE_IMAGE_PATH`: image path to apply to hallucinate scene.
* `CHECKPOINT_PATH`: path to checkpoint.
* `OUT_PATH`: path to the output directory with NeRF based rendered images for poses from config and exported video.

### [Video exporting result](#contents)

Results are the rendered images for passed poses in pose config stored in the `OUT_PATH` directory
and the exported video is based on the rendered poses.

## [3D mesh exporting](#contents)

### [3D mesh exporting process](#contents)

#### [3D mesh exporting on GPU](#contents)

```bash
bash scripts/run_export_3d.sh [TRAIN_CONFIG] [CHECKPOINT_PATH] [OUT_STL]
```

Script parameters:

* `TRAIN_CONFIG`: training loading settings config.
* `CHECKPOINT_PATH`: path to checkpoint.
* `OUT_STL`: output stl file for storing the 3d model.

### [3D mesh exporting result](#contents)

Result is the STL file with internal 3D mesh representation based on the coarse Ha-NeRF model raw output.
It is useful for synthetic scenes like blender dataset.

## [Model Description](#contents)

### [Performance](#contents)

| Parameters          | GPU                                                              |
| ------------------- |------------------------------------------------------------------|
| Model Version       | HA-NeRF                                                          |
| Resource            | 1xGPU(NVIDIA GeForce RTX 3090), CPU 2.1GHz 64 cores, Memory 256G |
| Uploaded Date       | 08/30/2023 (month/day/year)                                      |
| MindSpore Version   | 2.0.0.dev20230609                                                |
| Dataset             | Phototourism                                                     |
| Training Parameters | batch_size = 1024, epochs number depends on the dataset          |
| Optimizer           | Adam                                                             |
| Loss Function       | MSE, L2, MAE                                                     |
| Speed               | 1024 rays per batch: 310.559 ms                                  |
| Metric              | Mean PSNR of predicted images                                    |
| PSNR                | Brandenburg gate - 24.2449, Trevi fountain - 19.9868             |
| Configuration       | [link](src/configs/nerf_config.json)                             |

## [Description of Random Situation](#contents)

There is no seed turned on.

## [ModelZoo Homepage](#contents)

Please check the official [homepage](https://gitee.com/mindspore/models).