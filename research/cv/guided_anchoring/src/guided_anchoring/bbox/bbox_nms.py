# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
from typing import Dict, Optional, Tuple

import mindspore as ms
import mindspore.numpy as np
import mindspore.ops as ops
from mindspore.ops import (
    NMSWithMask,
    Sort,
    broadcast_to,
    concat,
)


def multiclass_nms(multi_bboxes,
                   multi_scores,
                   score_thr,
                   nms_cfg,
                   max_num=-1,
                   score_factors=None,
                   return_inds=False):
    """
    NMS for multi-class bboxes.

    Args:
    ----
        multi_bboxes (Tensor): shape (n, #class*4) or (n, 4)
        multi_scores (Tensor): shape (n, #class), where the last column
            contains scores of the background class, but this will be ignored.
        score_thr (float): bbox threshold, bboxes with scores lower than it
            will not be considered.
        nms_cfg (dict): a dict that contains the arguments of nms operations
        max_num (int, optional): if there are more than max_num bboxes after
            NMS, only top max_num will be kept. Default to -1.
        score_factors (Tensor, optional): The factors multiplied to scores
            before applying NMS. Default to None.
        return_inds (bool, optional): Whether return the indices of kept
            bboxes. Default to False.

    Returns:
    -------
        tuple: (dets, labels, indices (optional)), tensors of shape (k, 5),
            (k), and (k). Dets are boxes with scores. Labels are 0-based.
    """
    num_classes = multi_scores.shape[1] - 1
    # exclude background category
    if multi_bboxes.shape[1] > 4:
        bboxes = multi_bboxes.view((multi_scores.shape[0], -1, 4))
    else:
        bboxes = broadcast_to(multi_bboxes[:, None], (multi_scores.shape[0], num_classes, 4))

    scores = multi_scores[:, :-1]
    labels = np.arange(num_classes, dtype=ms.int64)
    labels = labels.view((1, -1)).expand_as(scores)
    bboxes = bboxes.reshape((-1, 4))
    scores = scores.reshape(-1)
    labels = labels.reshape(-1)
    valid_mask = scores > score_thr

    # NonZero not supported  in TensorRT
    inds = valid_mask.nonzero().squeeze(1)
    bboxes, scores, labels = bboxes[inds], scores[inds], labels[inds]

    if bboxes.size == 0:
        dets = concat([bboxes, scores[:, None]], -1)
        if return_inds:
            return dets, labels, inds
        return dets, labels

    scores, inds_ = ms.ops.Sort(descending=True)(scores)
    bboxes = bboxes[inds_]
    labels = labels[inds_]
    dets, labels_ = batched_nms(bboxes, scores, labels, nms_cfg)
    if max_num > 0:
        dets = dets[:max_num]
        labels_ = labels_[:max_num]

    return dets, labels_


def batched_nms(boxes: ms.Tensor,
                scores: ms.Tensor,
                idxs: ms.Tensor,
                nms_cfg: Optional[Dict],
                class_agnostic: bool = False
                ) -> Tuple[ms.Tensor, ms.Tensor]:
    r"""
    Performs non-maximum suppression in a batched fashion.

    Modified from `torchvision/ops/boxes.py#L39
    <https://github.com/pytorch/vision/blob/
    505cd6957711af790211896d32b40291bea1bc21/torchvision/ops/boxes.py#L39>`_.
    In order to perform NMS independently per class, we add an offset to all
    the boxes. The offset is dependent only on the class idx, and is large
    enough so that boxes from different classes do not overlap.

    Note:
    ----
        In v1.4.1 and later, ``batched_nms`` supports skipping the NMS and
        returns sorted raw results when `nms_cfg` is None.

    Args:
    ----
        boxes (ms.Tensor): boxes in shape (N, 4) or (N, 5).
        scores (ms.Tensor): scores in shape (N, ).
        idxs (ms.Tensor): each index value correspond to a bbox cluster,
            and NMS will not be applied between elements of different idxs,
            shape (N, ).
        nms_cfg (dict | optional): Supports skipping the nms when `nms_cfg`
            is None, otherwise it should specify nms type and other
            parameters like `iou_thr`. Possible keys includes the following.

            - iou_threshold (float): IoU threshold used for NMS.
            - split_thr (float): threshold number of boxes. In some cases the
              number of boxes is large (e.g., 200k). To avoid OOM during
              training, the users could set `split_thr` to a small value.
              If the number of boxes is greater than the threshold, it will
              perform NMS on each group of boxes separately and sequentially.
              Defaults to 10000.
        class_agnostic (bool): if true, nms is class agnostic,
            i.e. IoU thresholding happens over all boxes,
            regardless of the predicted class. Defaults to False.

    Returns:
    -------
        tuple: kept dets and indice.

        - boxes (Tensor): Bboxes with score after nms, has shape
          (num_bboxes, 5). last dimension 5 arrange as
          (x1, y1, x2, y2, score)
        - keep (Tensor): The indices of remaining boxes in input
          boxes.
    """
    # skip nms when nms_cfg is None
    if nms_cfg is None:
        scores, inds = Sort(descending=True)(scores)
        boxes = boxes[inds]
        return concat([boxes, scores[:, None]], -1), inds

    class_agnostic = nms_cfg.get('class_agnostic', class_agnostic)
    if class_agnostic:
        boxes_for_nms = boxes
    else:
        boxes_for_nms = boxes

    nms_op = NMSWithMask(nms_cfg['iou_threshold'])

    max_num = nms_cfg.get('max_num', -1)

    total_mask = np.zeros(scores.shape, dtype='bool')
    scores_after_nms = ops.zeros_like(scores)
    labels_after_nms = ops.zeros_like(scores)
    inuque_labels = ops.unique(idxs)[0]
    for i in range(inuque_labels.shape[0]):
        _id = inuque_labels[i]

        mask = (idxs == _id).nonzero().view(-1)

        nms_input = ops.concat(
            [boxes_for_nms[mask], ops.expand_dims(scores[mask], 1)],
            -1
        )
        boxes, indices, mask_ = nms_op(ms.Tensor(nms_input.asnumpy()))
        keep = indices[mask_.nonzero().squeeze(1)]
        total_mask[mask[keep]] = True
        scores_after_nms[mask[keep]] = boxes[:, -1][keep]
        boxes_for_nms[mask[keep]] = boxes[:, :4][keep]
        labels_after_nms[mask[keep]] = _id

    scores, inds_ = ms.ops.Sort(descending=True)(scores_after_nms)
    boxes_for_nms = boxes_for_nms[inds_]
    labels_after_nms = labels_after_nms[inds_]
    if max_num > 0:
        scores = scores[:max_num]
        boxes_for_nms = boxes_for_nms[:max_num]
        labels_after_nms = labels_after_nms[:max_num]

    return concat([boxes_for_nms, scores[:, None]], -1), labels_after_nms
