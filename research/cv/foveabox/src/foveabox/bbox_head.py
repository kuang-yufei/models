# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# ============================================================================

import mindspore as ms
import mindspore.ops as ops
from mindspore import nn

from .base_dense_head import BaseDenseHead
from .conv_module import ConvModule
from .deform_conv2d import DeformConv2d
from .loss import SigmoidFocalClassificationLoss
from .point_generator import MlvlPointGenerator


class FeatureAlign(nn.Cell):

    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size=3,
                 deform_groups=4):
        super().__init__()
        offset_channels = kernel_size * kernel_size * 2
        self.kernel_size = kernel_size
        self.deform_groups = deform_groups
        self.conv_offset = nn.Conv2d(
            in_channels=4,
            out_channels=deform_groups * offset_channels,
            kernel_size=1,
            has_bias=False
        )

        self.conv_adaption = DeformConv2d(
            in_channels,
            out_channels,
            kernel_size=kernel_size,
            padding=(kernel_size - 1) // 2,
            deform_groups=deform_groups,
        )
        self.relu = nn.ReLU()

    def construct(self, x, shape):
        offset = self.conv_offset(shape)
        x = self.relu(self.conv_adaption(x, offset))
        return x


class FoveaHead(BaseDenseHead):
    """FoveaBox: Beyond Anchor-based Object Detector
    https://arxiv.org/abs/1904.03797
    """

    def __init__(self,
                 num_classes,
                 in_channels,
                 strides,
                 img_width,
                 img_height,
                 batch_size: int,
                 loss_cls: SigmoidFocalClassificationLoss,
                 loss_bbox: nn.SmoothL1Loss,
                 base_edge_list=(16, 32, 64, 128, 256),
                 scale_ranges=((8, 32), (16, 64),
                               (32, 128), (64, 256),
                               (128, 512)),
                 sigma=0.4,
                 with_deform=False,
                 deform_groups=4,
                 stacked_convs=4,
                 use_sigmoid_cls=True,
                 dcn_on_last_conv=False,
                 conv_bias='auto',
                 conv_cfg=None,
                 norm_cfg=None,
                 test_cfg=None,
                 feat_channels=256
                 ):
        super().__init__()
        self.in_channels = in_channels
        self.prior_generator = MlvlPointGenerator(strides)
        self.strides = strides
        self.base_edge_list = base_edge_list
        self.scale_ranges = scale_ranges
        self.sigma = sigma
        self.with_deform = with_deform
        self.deform_groups = deform_groups
        self.num_classes = num_classes
        if use_sigmoid_cls:
            self.cls_out_channels = num_classes
        else:
            self.cls_out_channels = num_classes + 1
        self.stacked_convs = stacked_convs
        self.dcn_on_last_conv = dcn_on_last_conv
        assert conv_bias == 'auto' or isinstance(conv_bias, bool)
        self.conv_bias = conv_bias
        if hasattr(conv_cfg, 'as_dict'):
            conv_cfg = conv_cfg.as_dict()
        self.conv_cfg = conv_cfg
        if hasattr(norm_cfg, 'as_dict'):
            norm_cfg = norm_cfg.as_dict()
        self.norm_cfg = norm_cfg
        if hasattr(test_cfg, 'as_dict'):
            test_cfg = test_cfg.as_dict()
        self.test_cfg = test_cfg
        self.feat_channels = feat_channels
        # To avoid computing these shapes in runtime
        self.featmap_sizes = [(img_height // d, img_width // d)
                              for d in (8, 16, 32, 64, 128)]
        self.tensor_featmap_sizes = ms.Tensor(self.featmap_sizes,
                                              dtype=ms.int32)

        self.test_topk = ops.TopK(sorted=True)
        self.num_levels = 5

        self.loss_cls = loss_cls
        self.loss_bbox = loss_bbox

        self.batch_size = batch_size

        self._init_layers()

    def _init_cls_convs(self):
        """Initialize classification conv layers of the head."""
        self.cls_convs = nn.CellList()
        for i in range(self.stacked_convs):
            chn = self.in_channels if i == 0 else self.feat_channels
            if self.dcn_on_last_conv and i == self.stacked_convs - 1:
                conv_cfg = dict(type='DCNv2')
            else:
                conv_cfg = self.conv_cfg
            self.cls_convs.append(
                ConvModule(
                    chn,
                    self.feat_channels,
                    3,
                    stride=1,
                    padding=1,
                    conv_cfg=conv_cfg,
                    norm_cfg=self.norm_cfg,
                    bias=self.conv_bias
                )
            )

    def _init_reg_convs(self):
        """Initialize bbox regression conv layers of the head."""
        self.reg_convs = nn.CellList()
        for i in range(self.stacked_convs):
            chn = self.in_channels if i == 0 else self.feat_channels
            if self.dcn_on_last_conv and i == self.stacked_convs - 1:
                conv_cfg = dict(type='DCNv2')
            else:
                conv_cfg = self.conv_cfg

            self.reg_convs.append(
                ConvModule(
                    chn,
                    self.feat_channels,
                    3,
                    stride=1,
                    padding=1,
                    conv_cfg=conv_cfg,
                    norm_cfg=self.norm_cfg,
                    bias=self.conv_bias
                )
            )

    def _init_layers(self):
        # box branch
        self._init_reg_convs()
        self.conv_reg = nn.Conv2d(
            in_channels=self.feat_channels,
            out_channels=4,
            kernel_size=3,
            pad_mode='pad',
            padding=1,
            has_bias=True
        )

        # cls branch
        if not self.with_deform:
            self._init_cls_convs()
            self.conv_cls = nn.Conv2d(
                in_channels=self.feat_channels,
                out_channels=self.cls_out_channels,
                kernel_size=3,
                pad_mode='pad',
                padding=1,
                has_bias=True
            )
        else:
            self.cls_convs = nn.CellList()
            self.cls_convs.append(
                ConvModule(
                    self.feat_channels, (self.feat_channels * 4),
                    3,
                    stride=1,
                    padding=1,
                    conv_cfg=self.conv_cfg,
                    norm_cfg=self.norm_cfg,
                    bias=self.norm_cfg is None
                )
            )
            self.cls_convs.append(
                ConvModule(
                    (self.feat_channels * 4),
                    (self.feat_channels * 4),
                    1,
                    stride=1,
                    padding=0,
                    conv_cfg=self.conv_cfg,
                    norm_cfg=self.norm_cfg,
                    bias=self.norm_cfg is None
                )
            )
            self.feature_adaption = FeatureAlign(
                self.feat_channels,
                self.feat_channels,
                kernel_size=3,
                deform_groups=self.deform_groups)

            self.conv_cls = nn.Conv2d(
                in_channels=int(self.feat_channels * 4),
                out_channels=self.cls_out_channels,
                kernel_size=3,
                pad_mode='pad',
                padding=1,
                has_bias=True
            )

    def construct(self, inputs):
        res_cls = []
        res_reg = []
        for k in range(len(inputs)):
            # Forward single
            cls_feat = inputs[k]
            reg_feat = inputs[k]
            for reg_layer in self.reg_convs:
                reg_feat = reg_layer(reg_feat)
            bbox_pred = self.conv_reg(reg_feat)
            res_reg.append(bbox_pred)

            if self.with_deform:
                cls_feat = self.feature_adaption(cls_feat, bbox_pred.exp())
            for cls_layer in self.cls_convs:
                cls_feat = ops.relu(cls_layer(cls_feat))
            cls_score = self.conv_cls(cls_feat)
            res_cls.append(cls_score)
        return res_cls, res_reg

    def loss(self,
             cls_scores,
             bbox_preds,
             gt_bbox_list,
             gt_label_list,
             img_metas,
             gt_bboxes_ignore=None):
        assert len(cls_scores) == len(bbox_preds)
        points = self.prior_generator.grid_priors(self.tensor_featmap_sizes)

        num_imgs = cls_scores[0].shape[0]
        flatten_cls_scores = [
            cls_score.permute(0, 2, 3, 1).reshape(-1, self.cls_out_channels)
            for cls_score in cls_scores
        ]
        flatten_bbox_preds = [
            bbox_pred.permute(0, 2, 3, 1).reshape(-1, 4)
            for bbox_pred in bbox_preds
        ]
        flatten_cls_scores = ops.concat(flatten_cls_scores)
        flatten_bbox_preds = ops.concat(flatten_bbox_preds)

        flatten_labels, flatten_bbox_targets = self.get_targets(
            gt_bbox_list, gt_label_list, self.featmap_sizes, points)

        # FG cat_id: [0, num_classes -1], BG cat_id: num_classes
        pos_mask = ((flatten_labels >= 0) & (flatten_labels < self.num_classes))
        pos_num = ops.sum(pos_mask.astype(ms.int32))

        loss_cls = self.loss_cls(
            flatten_cls_scores, flatten_labels,
            avg_factor=pos_num + num_imgs)

        pos_mask = pos_mask.expand_dims(-1).tile((1, 4))
        pos_bbox_preds = ops.masked_fill(
            flatten_bbox_preds,
            ~pos_mask,
            1
        )
        pos_bbox_targets = ops.masked_fill(
            flatten_bbox_targets,
            ~pos_mask,
            1
        )
        pos_weights = pos_bbox_targets.new_zeros(
            pos_bbox_targets.shape) + 1.0

        loss_bbox = self.loss_bbox(
            pos_bbox_preds,
            pos_bbox_targets)

        loss_bbox = (
            ops.reduce_sum(pos_weights * loss_bbox) /
            (pos_num + ms.Tensor(1e-7, ms.float32))
        )
        return loss_cls, loss_bbox

    def get_targets(self, gt_bbox_list, gt_label_list, featmap_sizes, points):
        num_fpn = 5
        label_list = [[] for _ in range(num_fpn)]
        bbox_target_list = [[] for _ in range(num_fpn)]
        for i in range(self.batch_size):
            # level_labels: 5 of HxW
            # level_bbox_targets: 5 of HxWx4
            level_labels, level_bbox_targets = self._get_target_single(
                gt_bbox_list[i],
                gt_label_list[i],
                point_list=points
            )
            for j in range(num_fpn):
                label_list[j].append(level_labels[j].astype(ms.float64))
                bbox_target_list[j].append(level_bbox_targets[j].astype(ms.float64))

        flatten_labels = [
            ops.cat(labels_level) for labels_level in label_list
        ]
        flatten_bbox_targets = [
            ops.cat(bbox_targets_level) for bbox_targets_level in bbox_target_list
        ]
        flatten_labels = ops.cat(flatten_labels)
        flatten_bbox_targets = ops.cat(flatten_bbox_targets)
        return flatten_labels.astype(ms.int64), flatten_bbox_targets.astype(ms.float32)

    def _get_target_single(self,
                           gt_bboxes_raw,
                           gt_labels_raw,
                           point_list=None):

        gt_areas = ops.sqrt((gt_bboxes_raw[:, 2] - gt_bboxes_raw[:, 0]) *
                            (gt_bboxes_raw[:, 3] - gt_bboxes_raw[:, 1]))
        label_list = []
        bbox_target_list = []
        # for each pyramid, find the cls and box target
        n_fpn = 5
        for n in range(n_fpn):
            base_len = self.base_edge_list[n]
            (lower_bound, upper_bound) = self.scale_ranges[n]
            stride = self.strides[n]
            featmap_size = self.featmap_sizes[n]
            points = point_list[n]
            # FG cat_id: [0, num_classes -1], BG cat_id: num_classes
            points2 = points.reshape((featmap_size[0], featmap_size[1], 2))
            x, y = points2[..., 0], points2[..., 1]
            labels = ops.zeros((100, featmap_size[0], featmap_size[1]), dtype=ms.int64)
            bbox_targets = ops.zeros(
                (100, featmap_size[0], featmap_size[1], 4),
                dtype=ms.float32)
            # scale assignment
            hit_mask = ((gt_areas >= lower_bound) & (gt_areas <= upper_bound))
            _, hit_index_order = ops.sort(-ops.masked_fill(gt_areas,
                                                           ~hit_mask,
                                                           0))
            hit_mask = hit_mask[hit_index_order]
            gt_bboxes = ops.masked_fill(gt_bboxes_raw[hit_index_order],
                                        ~hit_mask.expand_dims(-1).tile((1, 4)),
                                        0) / stride
            gt_labels = ops.masked_fill(gt_labels_raw[hit_index_order],
                                        ~hit_mask.expand_dims(-1),
                                        0)

            half_w = 0.5 * (gt_bboxes[:, 2] - gt_bboxes[:, 0])
            half_h = 0.5 * (gt_bboxes[:, 3] - gt_bboxes[:, 1])
            # valid fovea area: left, right, top, down
            pos_left = ops.ceil(
                gt_bboxes[:, 0] + (1 - self.sigma) * half_w - 0.5).long(). \
                clamp(0, featmap_size[1] - 1)
            pos_right = ops.floor(
                gt_bboxes[:, 0] + (1 + self.sigma) * half_w - 0.5).long(). \
                clamp(0, featmap_size[1] - 1)
            pos_top = ops.ceil(
                gt_bboxes[:, 1] + (1 - self.sigma) * half_h - 0.5).long(). \
                clamp(0, featmap_size[0] - 1)
            pos_down = ops.floor(
                gt_bboxes[:, 1] + (1 + self.sigma) * half_h - 0.5).long(). \
                clamp(0, featmap_size[0] - 1)

            y_idx, x_idx = ops.meshgrid(
                ops.arange(0, self.featmap_sizes[n][0], 1),
                ops.arange(0, self.featmap_sizes[n][1], 1),
                indexing='ij'
            )
            x_idx = x_idx.expand_dims(-1).tile((1, 1, 100))
            y_idx = y_idx.expand_dims(-1).tile((1, 1, 100))

            m1 = x_idx >= pos_left
            m2 = x_idx < pos_right + 1
            m3 = y_idx >= pos_top
            m4 = y_idx < pos_down + 1
            m12 = ops.logical_and(m1, m2)
            m34 = ops.logical_and(m3, m4)
            update_masks = ops.logical_and(m12, m34)

            update_masks = ops.logical_and(
                update_masks,
                hit_mask.reshape((1, 1, -1)).tile((featmap_size[0],
                                                   featmap_size[1], 1))
            ).transpose((2, 0, 1))

            cumsum = ops.cumsum(update_masks.astype(ms.float32), axis=0)
            update_masks = ops.logical_and(update_masks, cumsum == cumsum[-1])

            gt_x1, gt_y1, gt_x2, gt_y2 = ops.split(
                ops.masked_fill(gt_bboxes_raw[hit_index_order],
                                ~hit_mask.expand_dims(-1).tile((1, 4)),
                                0),
                (1, 1, 1, 1),
                axis=1
            )
            x = x.expand_dims(-1).tile((1, 1, 100)).transpose((2, 1, 0))
            y = y.expand_dims(-1).tile((1, 1, 100)).transpose((2, 1, 0))

            update_1 = (x - gt_x1.expand_dims(-1).tile((
                1, featmap_size[0], featmap_size[1]
            ))) / base_len
            update_2 = (y - gt_y1.expand_dims(-1).tile((
                1, featmap_size[0], featmap_size[1]
            ))) / base_len
            update_3 = (gt_x2.expand_dims(-1).tile((
                1, featmap_size[0], featmap_size[1]
            )) - x) / base_len
            update_4 = (gt_y2.expand_dims(-1).tile((
                1, featmap_size[0], featmap_size[1]
            )) - y) / base_len
            updates = ops.stack(
                (update_1, update_2, update_3, update_4),
                axis=-1
            ).transpose(0, 2, 1, 3)
            labels = ops.select(
                update_masks,
                gt_labels.expand_dims(-1).tile((
                    1, featmap_size[0], featmap_size[1]
                )).astype(ms.int64),
                labels
            )
            labels = ops.sum(labels, dim=0)

            labels = ops.masked_fill(
                labels, ops.prod((~update_masks).astype(ms.int32), axis=0).astype(ms.bool_),
                self.num_classes
            )

            update_masks = update_masks.expand_dims(-1).tile((1, 1, 1, 4))
            bbox_targets = ops.select(update_masks,
                                      updates,
                                      bbox_targets)

            bbox_targets = ops.sum(bbox_targets, dim=0)

            bbox_targets = ops.masked_fill(
                bbox_targets, ops.prod((~update_masks).astype(ms.int32), axis=0).astype(ms.bool_),
                1
            )
            bbox_targets = bbox_targets.clamp(min=1. / 16, max=16.)
            label_list.append(labels.flatten())
            bbox_target_list.append(ops.log(bbox_targets).reshape(-1, 4))
        return label_list, bbox_target_list
