# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
Run prediction and save results as pickle.
"""

import pickle
import hashlib
import argparse
import warnings
from pathlib import Path

import cv2
import numpy as np
import mindspore as ms
import mindspore.nn as nn
from tqdm import tqdm


def parse_args():
    """
    Create and parse command-line arguments.

    Returns
    -------
    argparse.Namespace
        Parsed command-line arguments.

    """
    parser = argparse.ArgumentParser(
        description=__doc__, add_help=False,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument('--help', action='help',
                        default=argparse.SUPPRESS,
                        help='Show this help message and exit.')
    parser.add_argument('dataset', type=Path,
                        help='Path to dataset for prediction.')
    parser.add_argument('-c', '--checkpoint', type=Path,
                        help='Path to checkpoint to load.')
    parser.add_argument('-p', '--path', type=Path, required=True,
                        help='Path to the script with model or '
                             'the model itself in MindIR format.')
    parser.add_argument('-n', '--model-name',
                        help='Model name or function to get model '
                             'as in the script.')
    parser.add_argument('-i', '--init-params', default='',
                        help='Model initialization parameters.')
    parser.add_argument('-o', '--output', type=Path, default=Path('tst.pkl'),
                        help='Path to output PKL file.')
    parser.add_argument('-w', '--image-width', type=int, default=224,
                        help='Image width.')
    parser.add_argument('-h', '--image-height', type=int, default=224,
                        help='Image height.')
    parser.add_argument('-m', '--mode', help='Mode which should be set.',
                        choices=['graph', 'pynative'], default='graph')
    parser.add_argument('-d', '--device', help='Device to run network.',
                        choices=['CPU', 'GPU'], default='GPU')
    parser.add_argument('-f', '--kernel-fusion', action='store_true',
                        help='Enabling graph kernel fusion.')
    parser.add_argument('--num-classes', type=int, default=80,
                        help='Number of classes.')
    parser.add_argument('--norm-mean', type=float, nargs=3,
                        default=[123.675, 116.28, 103.53],
                        help='Normalization mean.')
    parser.add_argument('--norm-std', type=float, nargs=3,
                        default=[58.395, 57.12, 57.375],
                        help='Normalization standard deviation.')
    parser.add_argument('--keep-ratio', action='store_true',
                        help='Keep aspect ratio during image resizing.')
    parser.add_argument('--to-rgb', action='store_true')
    parser.add_argument('--no-rgb', action='store_false', dest='to_rgb')
    parser.set_defaults(to_rgb=True)

    return parser.parse_args()


def resize_img(img, img_width, img_height, keep_ratio=False):
    if keep_ratio:
        img_data, scale_factor = rescale_with_tuple(
            img, (img_width, img_height)
        )
        if img_data.shape[0] > img_height:
            img_data, scale_factor2 = rescale_with_tuple(
                img_data, (img_height, img_height)
            )
            scale_factor = scale_factor * scale_factor2
    else:
        img_data = img
        h, w = img_data.shape[:2]
        img_data = cv2.resize(
            img_data, (img_width, img_height),
            interpolation=cv2.INTER_LINEAR)
        h_scale = img_height / h
        w_scale = img_width / w
        scale_factor = np.array(
            [w_scale, h_scale, w_scale, h_scale], dtype=np.float32
        )

    return img_data, scale_factor


def rescale_with_tuple(img, scale):
    h, w = img.shape[:2]
    scale_factor = min(max(scale) / max(h, w), min(scale) / min(h, w))
    new_size = int(w * float(scale_factor) + 0.5), int(
        h * float(scale_factor) + 0.5)
    rescaled_img = cv2.resize(img, new_size, interpolation=cv2.INTER_LINEAR)

    return rescaled_img, scale_factor


def pad_img(img, img_width, img_height):
    img_data = img
    pad_h = img_height - img_data.shape[0]
    pad_w = img_width - img_data.shape[1]
    assert ((pad_h >= 0) and (pad_w >= 0))
    pad_img_data = np.zeros((img_height, img_width, 3)).astype(img_data.dtype)
    pad_img_data[0:img_data.shape[0], 0:img_data.shape[1], :] = img_data
    return pad_img_data


def rescale_column_test(img, img_width, img_height, keep_ratio=False):
    """
    Rescale operation for image evaluation.
    """
    img_data, scale_factor = resize_img(img, img_width, img_height, keep_ratio)

    pad_img_data = pad_img(img_data, img_width, img_height)

    img_meta = img.shape[:2]
    img_meta = np.append(img_meta, img_data.shape[:2])
    img_meta = np.append(img_meta, pad_img_data.shape[:2])
    img_meta = np.asarray(img_meta, dtype=np.float32)

    return pad_img_data, img_meta


def imnormalize_column(img, img_mean, img_std, to_rgb=True):
    """imnormalize operation for image"""
    # Computed from random subset of ImageNet training images
    mean = np.asarray(img_mean)
    std = np.asarray(img_std)
    img_data = img.copy().astype(np.float32)
    if to_rgb:
        cv2.cvtColor(img_data, cv2.COLOR_BGR2RGB, img_data)  # inplace
    cv2.subtract(img_data, np.float64(mean.reshape(1, -1)), img_data)
    cv2.multiply(img_data, 1 / np.float64(std.reshape(1, -1)), img_data)
    img_data = img_data.astype(np.float32)
    return img_data


def imread(path):
    img_bgr = cv2.imread(path, cv2.IMREAD_UNCHANGED)
    img_rgb = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB)
    return img_rgb


def data_loader(path: Path, img_width, img_height, img_mean, img_std,
                keep_ratio=False, to_rgb=True):
    """Load image or images from folder in generator."""
    def process_img(img):
        img, meta = rescale_column_test(img, img_width, img_height, keep_ratio)
        img = imnormalize_column(img, img_mean, img_std, to_rgb)
        img = img.transpose(2, 0, 1).copy()
        img = img.astype(np.float32)
        return img, meta

    extensions = ('.png', '.jpg', '.jpeg')
    for item in path.iterdir():
        if item.is_dir():
            continue
        if item.suffix.lower() not in extensions:
            continue
        image = imread(str(item))
        hash_key = hashlib.md5(image).hexdigest()
        image = image[..., ::-1].copy()
        tensor, img_meta = process_img(image)

        yield (
            str(item), hash_key, ms.Tensor(img_meta[np.newaxis, :]),
            ms.Tensor(tensor[np.newaxis, :])
        )


def main():
    """
    Application entry point.
    """
    args = parse_args()

    run_mode = {
        'PYNATIVE': ms.PYNATIVE_MODE,
        'GRAPH': ms.GRAPH_MODE
    }[args.mode.upper()]
    ms.set_context(device_target=args.device,
                   mode=run_mode,
                   enable_graph_kernel=args.kernel_fusion)

    if args.path.suffix.lower() == '.py':
        if args.model_name is None:
            raise AttributeError(
                'Need to specify model name as in the script.'
            )
        rel_path = str(args.path.absolute().relative_to(
            Path(__file__).absolute().parent
        ))
        import_path = rel_path.replace('.py', '').replace('/', '.')
        exec(f'from {import_path} import {args.model_name}')  # noqa
        net = eval(f'{args.model_name}({args.init_params})')  # noqa
    elif args.path.suffix.lower() == '.mindir':
        ms.set_context(mode=ms.GRAPH_MODE)
        if args.mode != 'graph':
            warnings.warn('Mode has been set to `GRAPH`. '
                          '`mode` argument was ignored.')
        graph = ms.load(str(args.path))
        net = nn.GraphCell(graph)
    else:
        raise NotImplementedError(
            'The script can work only with models described '
            'in python script or models in MindIR format.'
        )

    if args.checkpoint:
        ms.load_checkpoint(str(args.checkpoint), net=net)
    net.set_train(False)

    data_generator = data_loader(
        args.dataset, args.image_width, args.image_height,
        args.norm_mean, args.norm_std, args.keep_ratio, args.to_rgb
    )

    predictions = {}
    for _, hash_key, meta, tensor in tqdm(data_generator):
        outputs = net(tensor, meta, None, None, None)
        bboxes, labels, masks = outputs

        bboxes_squee = np.squeeze(bboxes.asnumpy()[0, :, :])
        labels_squee = np.squeeze(labels.asnumpy()[0, :, :])
        masks_squee = np.squeeze(masks.asnumpy()[0, :, :])

        meta = meta.asnumpy()[0]

        bboxes_mask = bboxes_squee[masks_squee, :4]
        bboxes_mask[::, [0, 2]] = np.clip(
            bboxes_mask[::, [0, 2]], a_min=0, a_max=meta[3]
        )
        bboxes_mask[::, [1, 3]] = np.clip(
            bboxes_mask[::, [1, 3]], a_min=0, a_max=meta[2]
        )
        bboxes_mask[::, [0, 2]] /= meta[3]
        bboxes_mask[::, [1, 3]] /= meta[2]

        bboxes_mask = bboxes_mask.reshape(-1, 2, 2)
        confs_mask = bboxes_squee[masks_squee, 4]
        labels_mask = labels_squee[masks_squee]

        predictions[hash_key] = {}
        for label in range(args.num_classes - 1):
            predictions[hash_key].setdefault(label, {})
            predictions[hash_key][label].setdefault('bbox', [])
            predictions[hash_key][label].setdefault('confidence', [])

        for box, label, conf in zip(bboxes_mask, labels_mask, confs_mask):
            predictions[hash_key][label]['bbox'].append(box)
            predictions[hash_key][label]['confidence'].append(conf)

    with open(args.output, 'wb') as f:
        pickle.dump(predictions, f)


if __name__ == '__main__':
    main()
