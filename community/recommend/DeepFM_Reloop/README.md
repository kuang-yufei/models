# 目录

<!-- TOC -->

- [目录](#目录)
    - [Reloop概述](#Reloop概述)
    - [模型架构](#模型架构)
    - [数据集](#数据集)
    - [环境要求](#环境要求)
    - [快速入门](#快速入门)
    - [脚本说明](#脚本说明)
        - [脚本和样例代码](#脚本和样例代码)
        - [脚本参数](#脚本参数)
    - [训练过程](#训练过程)
        - [训练](#训练)
    - [推理过程](#推理过程)
        - [推理](#推理)
    - [模型描述](#模型描述)
        - [性能](#性能)
            - [评估性能](#评估性能)
    - [ModelZoo主页](#modelzoo主页)

<!-- /TOC -->

## Reloop概述

目前的模型训练过程只获取用户的反馈作为标签，而没有考虑到之前推荐中的错误。本文为推荐系统构建一个自纠正学习循环（ReLoop），从而从之前的推荐错误中学习知识。构建自定义损失来鼓励每个新模型版本在训练期间减少对先前模型版本的预测误差，其核心思想是利用前一次训练的预测结果来约束当前轮次训练的性能不能差于前一次，简单有效。

[论文](https://dl.acm.org/doi/pdf/10.1145/3477495.3531922):  [1] Cai G ,  Zhu J ,  Dai Q , et al. ReLoop: A Self-Correction Continual Learning Loop for Recommender Systems[J].  2022.

## 模型架构

推荐模型的训练循环首先从用户对公开项的隐式反馈中收集训练数据，即单击项作为正样本，未单击项作为负样本。
其次，使用此类数据训练一个排名模型。然后，新模型将更新到在线服务。在线推理阶段，通过模型对候选人进行评估，根据预测分数的排名向用户展示。
最后，在线点击事件将被记录在用户行为日志中，这反过来又会触发新的训练。这就形成了推荐系统的训练循环。  
自校正模块的设计为每个样本都有一个标签，由用户隐式反馈生成。模型推理时可以记录预测点击率，表示用户点击项目的概率。直观上，将样本的预测点击率与真实标签之间的差距定义为误差，这样就可以从每个样本的最后一个预测点击率中得到最后一个误差。对于当前误差来讲，它表示训练时当前预测与真实标签之间的差距。受人类从错误中学习的启发，预期模型在当前时刻的表现应比上一时刻版本更好。
本项目基于基本的DeepFM网络优化开发。DeepFM网络参考：https://gitee.com/mindspore/models/tree/master/official/recommend/DeepFM

## 数据集

- 数据集来源：Weiyu Cheng, Yanyan Shen, Linpeng Huang. Adaptive Factorization Network: Learning Adaptive-Order Feature Interactions. In AAAI'20, New York, NY, USA, February 07-12, 2020.
- 数据集：[MovieLens](https://github.com/WeiyuCheng/AFN-AAAI-20/tree/master/data/movielens)

## 环境要求

- 硬件（Ascend或GPU）
    - 使用Ascend或GPU处理器准备硬件环境。
- 框架
    - [MindSpore](https://www.mindspore.cn/install)
- 如需查看详情，请参见如下资源：
    - [MindSpore教程](https://www.mindspore.cn/tutorials/zh-CN/master/index.html)
    - [MindSpore Python API](https://www.mindspore.cn/docs/zh-CN/master/index.html)

## 脚本说明

### 脚本和样例代码

```reloop
.
└─DeepFM_Reloop
  ├─README.md
  ├─src
    ├─model_utils
      ├─__init__.py
      ├─config.py
      ├─device_target.py
      ├─local_adapter.py
      └─moxing_adapter.py
    ├─__init__.py                     # python init文件
    ├─callback.py                     # 定义回调功能
    ├─get_lr.py                       # 迭代设置学习率
    ├─preprocess_movielens.py        # 数据预处理为mindrecord格式
    ├─deepfm_reloop.py                # DeepFM+Reloop网络
    ├─AFN_data_preprocess.py         # MovieLens数据预处理
    ├─deepfm.py                       # DeepFM网络
    └─dataset.py                      # DeepFM创建数据集
  ├─eval.py                           # 评估网络
  ├─train_reloop.py                  # DeepFM+Reloop训练网络
  ├─default_config.yaml              # 模型参数配置文件
  └─train.py                          # DeepFM训练网络

```

### 脚本参数

```config
data_vocab_size: 90437
batch_size: 4096
train_epochs: 75
learning_rate: 0.001
data_emb_dim: 40
deep_layer_args: [[1024, 512, 256, 128], "relu"]
keep_prob: 0.9
convert_dtype: True
optimizer：Adam
```

更多配置细节请参考脚本/default_config.yaml。

### 准备数据集

- 您可以根据数据集链接自行从GitHub下载并预处理数据集。本项目数据集文件下载后保存在data_mov/AFN_data中：

```reloop
.
└─DeepFM_Reloop
   ├─data_mov
    └─AFN_data                         # 存放原始数据
     ├─test.libsvm                    # 推理数据
     ├─train.libsvm                   # 训练数据
     └─valid.libsvm                   # 验证数据
```

- 将原数据转换为MindRecord数据格式进行训练和评估：
    - 首先进入/src文件，执行

    ```python AFN_data_preprocess.py```

    完成数据处理第一步，生成数据集data.txt和test.txt于/data_mov/origin_data内。
    - 再执行  

    ```python preprocess_movielens.py```

    完成数据处理，生成mindrecord和mindrecord2于/data_mov内。

## 训练过程

### 训练

- 在default_config.yaml中设置选项，包括优化器、学习率和网络等超参数。  
- 启动deepfm训练文件：
  在主目录下执行

  ```python train.py```

  训练原mindspore的deepfm网络，生成的ckpt文件保存在/results/ckpt中。
- 启动reloop训练文件：主目录下执行

  ```python train_reloop.py```

  训练加载预训练模型ckpt后的DeepFM+Reloop网络。生成的DeepFM+Reloop的训练模型ckpt也存放在/results/ckpt中（会自动往后编号）。

  训练过程中能看到相应的训练损失和验证精度，结果分别保存在/results/loss.log和/results/auc.log中，过程展示如下：

  ```运行结果
    epoch: 1 step: 368, loss is 0.29086291790008545
    Train epoch time: 11579.596 ms, per step time: 31.466 ms
    2023-05-26 15:54:55 EvalCallBack metricdict_values([0.9382123725949632]); eval_time5s
    epoch: 2 step: 368, loss is 0.256080687046051
    Train epoch time: 1095.863 ms, per step time: 2.978 ms
    2023-05-26 15:54:57 EvalCallBack metricdict_values([0.9426950536984469]); eval_time0s
    ...
    epoch: 74 step: 368, loss is 0.023970333859324455
    Train epoch time: 1066.934 ms, per step time: 2.899 ms
    2023-05-26 15:56:57 EvalCallBack metricdict_values([0.9720733150262478]); eval_time0s
    epoch: 75 step: 368, loss is 0.02591869607567787  
    Train epoch time: 1056.790 ms, per step time: 2.872 ms
    2023-05-26 15:56:59 EvalCallBack metricdict_values([0.9720889017627561]); eval_time0s
  ```

## 推理过程

### 推理

- 启动推理：选择reloop训练的权重模型的ckpt路径，执行  

```python eval.py --checkpoint_path='results/ckpt/deepfm_reloop_1-75_368.ckpt'```

推理结果

```2023-05-26 16:06:38 AUC: 0.9722788319524431, eval time: 32.03981852531433s.```

## 模型描述

### 性能

#### 评估性能

| 参数                    | Ascend                                                      |
| -------------------------- | ----------------------------------------------------------- |
| 模型版本              | Reloop                                                      |
| 资源                   |910PremiumA             |
| 上传日期              | 2023-05-26                                 | 待运行                 |
| MindSpore版本          | 1.8.0                                                 |
| 数据集                    | MovieLens                                                         |
| 训练参数        | epoch=75, batch_size=4096                         |
| 优化器                  | Adam                                                        |
| 损失函数              | 详见论文                           |
| 输出                    | 准确率                                                    |
| 结果                    | 97.20%                                                    |
| 损失                       | 0.02006455324590206                                                        |
| 速度| 3ms/step                                          |
| 脚本                    | [Reloop脚本](https://gitee.com/mindspore/models/tree/master/community/recommend/DeepFM_Reloop) |

## ModelZoo主页

 请浏览官网[主页](https://gitee.com/mindspore/models)。  
