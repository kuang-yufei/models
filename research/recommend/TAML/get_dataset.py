# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import random
import re

import joblib
import mindspore.dataset as ds
import numpy as np

random.seed(2020)
np.random.seed(2020)
sample_path = 'data/sample_skeleton_{}.csv'
common_path = 'data/common_features_{}.csv'
enum_path = 'data/ctrcvr_enum.pkl'
write_path = 'data/ctr_cvr'
filter_theresold = 10
feats_columns = [
    '101',
    '121',
    '122',
    '124',
    '125',
    '126',
    '127',
    '128',
    '129',
    '205',
    '206',
    '207',
    '216',
    '508',
    '509',
    '702',
    '853',
    '301']


def read_train_map():
    common_dict = {}
    with open(common_path.format("train"), 'r') as fr:
        cnt = 0
        for line in fr:
            line_list = line.strip().split(',')
            index, key_value = line_list[0], line_list[2]
            kev = np.array(re.split('\x01|\x02|\x03', key_value))
            feats_dict = {}
            for i in range(0, len(kev), 3):
                feats_dict[kev[i]] = kev[i + 1]
            common_dict[index] = feats_dict
            cnt += 1
            if cnt % 100000 == 0:
                print(cnt)
    return common_dict

def write_feats(common_dict, voc):
    with open(sample_path.format("train") + '.tmp', 'w') as fw:
        fw.write('click,purchase,' + ','.join(feats_columns) + '\n')
        with open(sample_path.format("train"), 'r') as fr:
            cnt = 0
            for line in fr:
                line_list = line.strip().split(',')
                click, purchase, index, key_value = line_list[1], line_list[2], line_list[3], line_list[5]
                if click == '0' and purchase == '1':
                    continue
                kev = np.array(re.split('\x01|\x02|\x03', key_value))
                feats_dict = {}
                for i in range(0, len(kev), 3):
                    feats_dict[kev[i]] = kev[i + 1]
                feats_dict.update(common_dict[index])
                feats = [click, purchase]
                for key in feats_columns:
                    if key in feats_dict:
                        feats.append(feats_dict[key])
                    else:
                        feats.append('0')
                fw.write(','.join(feats) + '\n')
                for key, value in feats_dict.items():
                    if key not in feats_columns:
                        continue
                    if value not in voc[key]:
                        voc[key][value] = 0
                    else:
                        voc[key][value] += 1
                cnt += 1
                if cnt % 100000 == 0:
                    print(cnt)

def get_map():
    voc = {}
    for column in feats_columns:
        voc[column] = {}

    filter_voc = {}
    for column in feats_columns:
        filter_voc[column] = set()

    print('feats loading...')
    common_dict = read_train_map()

    print('feats joining...')
    write_feats(common_dict=common_dict, voc=voc)

    print('freq before filter:')
    for key, value in voc.items():
        print(key, ':', len(value))
    for key, value in voc.items():
        for key_in, value_in in value.items():
            if value_in > filter_theresold:
                filter_voc[key].add(key_in)
    voc = filter_voc
    print('freq after filter:')
    for key, value in voc.items():
        print(key, ':', len(value))
    joblib.dump(voc, enum_path, compress=3)


def process_ccp(mode='train'):
    print('feats loading...')
    common_dict = {}
    with open(common_path.format("test"), 'r') as fr:
        cnt = 0
        for line in fr:
            line_list = line.strip().split(',')
            index, key_value = line_list[0], line_list[2]
            kev = np.array(re.split('\x01|\x02|\x03', key_value))
            feats_dict = {}
            for i in range(0, len(kev), 3):
                feats_dict[kev[i]] = kev[i + 1]
            common_dict[index] = feats_dict
            cnt += 1
            if cnt % 100000 == 0:
                print(cnt)

    print('feats joining...')
    with open(sample_path.format("test") + '.tmp', 'w') as fw:
        fw.write('click,purchase,' + ','.join(feats_columns) + '\n')
        with open(sample_path.format("test"), 'r') as fr:
            cnt = 0
            for line in fr:
                line_list = line.strip().split(',')
                click, purchase, index, key_value = line_list[1], line_list[2], line_list[3], line_list[5]
                if click == '0' and purchase == '1':
                    continue
                kev = np.array(re.split('\x01|\x02|\x03', key_value))
                feats_dict = {}
                for i in range(0, len(kev), 3):
                    feats_dict[kev[i]] = kev[i + 1]
                feats_dict.update(common_dict[index])
                feats = [click, purchase]
                for key in feats_columns:
                    if key in feats_dict:
                        feats.append(feats_dict[key])
                    else:
                        feats.append('0')
                fw.write(','.join(feats) + '\n')
                cnt += 1
                if cnt % 100000 == 0:
                    print(cnt)

    print('encode feats...')
    voc = joblib.load(enum_path)
    feats_map = {}
    for feat in feats_columns:
        feats_map[feat] = dict(zip(voc[feat], range(1, len(voc[feat]) + 1)))

    with open(write_path + '.train', 'w') as fw1:
        fw1.write('click,purchase,' + ','.join(feats_columns) + '\n')
        with open(write_path + '.dev', 'w') as fw2:
            fw2.write('click,purchase,' + ','.join(feats_columns) + '\n')
            with open(sample_path.format('train') + '.tmp', 'r') as fr:
                fr.readline()
                cnt = 0
                for line in fr:
                    line_list = line.strip().split(',')
                    click, purchase, value = line_list[0], line_list[1], line_list[2:]
                    new_line = [click, purchase]
                    for idx, feat in enumerate(feats_columns):
                        new_line.append(str(feats_map[feat].get(value[idx], '0')))
                    if random.random() <= 0.9:
                        fw1.write(','.join(new_line) + '\n')
                    else:
                        fw2.write(','.join(new_line) + '\n')
                    cnt += 1
                    if cnt % 100000 == 0:
                        print(cnt)

    with open(write_path + '.test', 'w') as fw:
        fw.write('click,purchase,' + ','.join(feats_columns) + '\n')
        with open(sample_path.format('test') + '.tmp', 'r') as fr:
            fr.readline()
            cnt = 0
            for line in fr:
                line_list = line.strip().split(',')
                click, purchase, value = line_list[0], line_list[1], line_list[2:]
                new_line = [click, purchase]
                for idx, feat in enumerate(feats_columns):
                    new_line.append(str(feats_map[feat].get(value[idx], '0')))
                fw.write(','.join(new_line) + '\n')
                cnt += 1
                if cnt % 100000 == 0:
                    print(cnt)


def get_ccp(datapath, batchsize):
    data = []
    label_ctr = []
    label_ctcvr = []
    with open(datapath, 'r') as fr:
        fr.readline()
        for line in fr:
            d = line.split(',')
            label_ctr.append(d[0])
            label_ctcvr.append(d[1])
            data.append(np.array(d[2:], dtype=np.int32))
    label_ctr = np.array(label_ctr, dtype=np.float32)
    label_ctcvr = np.array(label_ctcvr, dtype=np.float32)

    dataset = ds.NumpySlicesDataset((data, label_ctr, label_ctcvr), ["data", "label_ctr", "label_ctcvr"], shuffle=True)
    dataset = dataset.batch(batchsize)
    return dataset


if __name__ == '__main__':
    get_map()
    process_ccp()
