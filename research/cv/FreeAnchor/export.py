# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""export checkpoint file into air, onnx, mindir models"""
import argparse
import os

from pprint import pprint

import numpy as np
import mindspore as ms

from mindspore import Tensor

from src.data.config import (
    parse_yaml, parse_cli_to_yaml, merge, Config, compute_features_info
)

from src.models.freeanchor.freeanchor import build_model
from src.models.freeanchor.layers.retinanet import (
    RetinanetInferWithDecoder
)

from src.tools.cell import cast_amp
from src.tools.misc import pretrained


def get_config():
    """
    Get Config according to the yaml file and cli arguments.
    """
    parser = argparse.ArgumentParser(description='default name',
                                     add_help=False)
    current_dir = os.path.dirname(os.path.abspath(__file__))
    parser.add_argument('--config_path', type=str,
                        default=os.path.join(current_dir,
                                             'default_config.yaml'),
                        help='Config file path')
    parser.add_argument('--export_file_path',
                        help='Path to output file.')
    parser.add_argument('--export_file_format', choices=['MINDIR', 'ONNX'],
                        default='ONNX', help='Export format type')
    parser.add_argument('--export_ckpt_file', help='Path to model checkpoint.')
    path_args, _ = parser.parse_known_args()
    default, helper, choices = parse_yaml(path_args.config_path)
    args = parse_cli_to_yaml(parser=parser, cfg=default, helper=helper,
                             choices=choices, cfg_path=path_args.config_path)
    final_config = Config(merge(args, default))
    final_config = compute_features_info(final_config)
    pprint(final_config)
    print('Please check the above information for the configurations',
          flush=True)
    return final_config


def export_freeanchor():
    """ export_freeanchor """
    config = get_config()
    args = config

    ms.set_context(
        mode=ms.GRAPH_MODE, device_target=config.device_target,
        max_call_depth=2000
    )

    model = build_model(args)
    pretrained(args, model)
    net = RetinanetInferWithDecoder(model, args)
    cast_amp(net, args)
    net.set_train(False)

    img = Tensor(
        np.random.randn(1, 3, config.img_height, config.img_width), ms.float32
    )
    img_metas = Tensor(np.random.uniform(0.0, 1.0, size=[1, 6]), ms.float32)

    ms.export(
        net, img, img_metas, file_name=config.export_file_path,
        file_format=config.export_file_format,
    )


if __name__ == '__main__':
    export_freeanchor()
