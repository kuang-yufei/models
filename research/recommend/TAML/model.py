# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import mindspore.nn as nn
from mindspore.ops import cat, mean, reshape, stop_gradient, sigmoid, mul, div, binary_cross_entropy, L2Loss

from module import HTAKR, TOWER


class TAML(nn.Cell):
    """
    TAML Model Structure
    """

    def __init__(self, cfg, feats_conf):
        super().__init__()

        bottom_set = cfg.bottom_set
        tower_set = cfg.tower_set
        loss_set = cfg.loss_set

        self.bottom_set = bottom_set
        self.tower_set = tower_set
        self.loss_set = loss_set

        self.l2_loss = L2Loss()

        embedding_size = cfg.embedding_size

        expert_hidden_units = bottom_set.get('expert_hidden_units', [64])
        expert_keep_rate = bottom_set.get('expert_keep_rate', 0.0)
        expert_use_bn = bottom_set.get('expert_use_bn', False)

        gate_hidden_units = bottom_set.get('gate_hidden_units', [64])
        gate_keep_rate = bottom_set.get('gate_keep_rate', 0.9)
        gate_use_bn = bottom_set.get('gate_use_bn', False)

        feature_dims = bottom_set.get('feature_dims', 18)
        num_tasks = bottom_set.get('num_tasks', 2)

        general_expert_nums = bottom_set.get('general_expert_nums', 1)
        domain_expert_nums = bottom_set.get('domain_expert_nums', 1)
        learner_expert_nums = bottom_set.get('learner_expert_nums', 1)
        num_learners = bottom_set.get('num_learners', 1)

        self.embedding = nn.CellList([])
        for map_size in feats_conf:
            self.embedding.append(nn.Embedding(vocab_size=map_size, embedding_size=embedding_size))

        self.htakr = HTAKR(expert_hidden_units=expert_hidden_units, expert_keep_rate=expert_keep_rate,
                           expert_use_bn=expert_use_bn,
                           gate_hidden_units=gate_hidden_units, gate_keep_rate=gate_keep_rate,
                           gate_use_bn=gate_use_bn,
                           feature_dims=feature_dims * embedding_size, num_tasks=num_tasks,
                           num_learners=num_learners,
                           domain_expert_nums=domain_expert_nums,
                           general_expert_nums=general_expert_nums,
                           learner_expert_nums=learner_expert_nums)

        tower_hidden_units = tower_set.get('tower_hidden_units', [64, 32])
        tower_keep_rate = tower_set.get('tower_keep_rate', 1)
        tower_use_bn = tower_set.get('tower_use_bn', False)
        feature_dims = expert_hidden_units[-1]
        num_learners = num_learners * learner_expert_nums * num_tasks

        self.tower = TOWER(tower_hidden_units=tower_hidden_units, tower_keep_rate=tower_keep_rate,
                           tower_use_bn=tower_use_bn,
                           feature_dims=feature_dims,
                           num_learners=num_learners)

    def construct(self, x, labels_ctr, labels_ctcvr):

        embedding_out = [self.embedding[i](x[:, i]) for i in range(self.bottom_set["feature_dims"])]

        dnn_input = cat(embedding_out, axis=-1)

        bottom_out = self.htakr(dnn_input)

        tower_input = bottom_out

        task_out = self.tower(tower_input)

        tower_logits_list = task_out

        learner_logits_list = tower_logits_list
        num_learners = self.bottom_set.get('num_learners', 2)

        task_tensor_list = [cat(
            learner_logits_list[task_index * num_learners:
                                (task_index + 1) * num_learners],
            axis=-1) for task_index in range(self.bottom_set['num_tasks'])]

        task_logits_list = [reshape(mean(task_tensor, axis=-1),
                                    [-1]) for task_tensor in task_tensor_list]

        train_preds_ctr = sigmoid(task_logits_list[0])

        if self.loss_set['task_relation'] == 'product-ctcvr':
            train_preds_cvr = sigmoid(task_logits_list[1])
            stop_gd = self.loss_set.get('stop_gd', False)
            if stop_gd:
                task_0_copy = stop_gradient(train_preds_ctr)
                train_preds_ctcvr = mul(task_0_copy, train_preds_cvr)
            else:
                train_preds_ctcvr = mul(train_preds_ctr, train_preds_cvr)
        elif self.loss_set['task_relation'] == 'sequence-individual':
            train_preds_ctcvr = sigmoid(task_logits_list[1])
            train_preds_cvr = div(train_preds_ctcvr, train_preds_ctr)
        else:
            raise NotImplementedError('pls check')

        loss_ctr = binary_cross_entropy(train_preds_ctr, labels_ctr)
        loss_ctcvr = binary_cross_entropy(train_preds_ctcvr, labels_ctcvr)

        ctr_weight = self.loss_set.get('t1_weight', 1.0)
        ctcvr_weight = self.loss_set.get('t2_weight', 1.0)
        l2_reg = self.loss_set.get('l2', 2e-6)

        loss = ctr_weight * loss_ctr + ctcvr_weight * loss_ctcvr

        for embed in self.embedding:
            loss += l2_reg * self.l2_loss(embed.embedding_table.value())

        kd_alpha = self.loss_set.get('t1_kd_weight', None)
        kd_beta = self.loss_set.get('t2_kd_weight', None)
        if kd_alpha is not None and kd_beta is not None:
            task_1_copy = stop_gradient(train_preds_ctr)
            task_2_copy = stop_gradient(train_preds_cvr)
            num_learners = self.bottom_set['num_learners']
            task1_learner_logits_list = tower_logits_list[0: num_learners]
            task2_learner_logits_list = tower_logits_list[num_learners: 2 * num_learners]

            task1_kd_loss_list = [
                mean(binary_cross_entropy(sigmoid(reshape(task_branch, [-1])), task_1_copy))
                for task_branch in task1_learner_logits_list
            ]

            task2_kd_loss_list = [
                mean(binary_cross_entropy(sigmoid(reshape(task_branch, [-1])), task_2_copy))
                for task_branch in task2_learner_logits_list
            ]

            loss += kd_alpha * sum(task1_kd_loss_list) + kd_beta * sum(task2_kd_loss_list)

        return loss, train_preds_ctr, train_preds_ctcvr
