import sys
import re
import fnmatch
from collections import defaultdict
from copy import deepcopy

__all__ = ['list_loss', 'is_loss', 'loss_entrypoint', 'list_modules', 'is_loss_in_modules',
           'is_loss_default_key', 'has_loss_default_key', 'get_loss_default_value', 'is_loss_pretrained']

_module_to_loss = defaultdict(set)  # dict of sets to check membership of loss in module
_loss_to_module = {}  # mapping of loss names to module names
_loss_entrypoints = {}  # mapping of loss names to entrypoint fns
_loss_has_pretrained = set()  # set of loss names that have pretrained weight url present
_loss_default_cfgs = dict()  # central repo for loss default_cfgs


def register_loss(fn):
    # lookup containing module
    mod = sys.modules[fn.__module__]
    module_name_split = fn.__module__.split('.')
    module_name = module_name_split[-1] if module_name_split else ''

    # add loss to __all__ in module
    loss_name = fn.__name__
    if hasattr(mod, '__all__'):
        mod.__all__.append(loss_name)
    else:
        mod.__all__ = [loss_name]

    # add entries to registry dict/sets
    _loss_entrypoints[loss_name] = fn
    _loss_to_module[loss_name] = module_name
    _module_to_loss[module_name].add(loss_name)
    has_pretrained = False  # check if loss has a pretrained url to allow filtering on this
    if hasattr(mod, 'default_cfgs') and loss_name in mod.default_cfgs:
        # this will catch all loss that have entrypoint matching cfg key, but miss any aliasing
        # entrypoints or non-matching combos
        has_pretrained = 'url' in mod.default_cfgs[loss_name] and 'http' in mod.default_cfgs[loss_name]['url']
        _loss_default_cfgs[loss_name] = deepcopy(mod.default_cfgs[loss_name])
    if has_pretrained:
        _loss_has_pretrained.add(loss_name)
    return fn


def _natural_key(string_):
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_.lower())]


def list_loss(filters='', module='', pretrained=False, exclude_filters='', name_matches_cfg=False):
    """ Return list of available loss names, sorted alphabetically
    Args:
        filter (str) - Wildcard filter string that works with fnmatch
        module (str) - Limit loss selection to a specific sub-module (ie 'gen_efficientnet')
        pretrained (bool) - Include only loss with pretrained weights if True
        exclude_filters (str or list[str]) - Wildcard filters to exclude loss after including them with filter
        name_matches_cfg (bool) - Include only loss w/ loss_name matching default_cfg name (excludes some aliases)
    Example:
        loss_list('gluon_resnet*') -- returns all loss starting with 'gluon_resnet'
        loss_list('*resnext*, 'resnet') -- returns all loss with 'resnext' in 'resnet' module
    """
    if module:
        all_loss = list(_module_to_loss[module])
    else:
        all_loss = _loss_entrypoints.keys()
    if filters:
        loss = []
        include_filters = filters if isinstance(filters, (tuple, list)) else [filters]
        for f in include_filters:
            include_loss = fnmatch.filter(all_loss, f)  # include these loss
            if include_loss:
                loss = set(loss).union(include_loss)
    else:
        loss = all_loss
    if exclude_filters:
        if not isinstance(exclude_filters, (tuple, list)):
            exclude_filters = [exclude_filters]
        for xf in exclude_filters:
            exclude_loss = fnmatch.filter(loss, xf)  # exclude these loss
            if exclude_loss:
                loss = set(loss).difference(exclude_loss)
    if pretrained:
        loss = _loss_has_pretrained.intersection(loss)
    if name_matches_cfg:
        loss = set(_loss_default_cfgs).intersection(loss)
    return list(sorted(loss, key=_natural_key))


def is_loss(loss_name):
    """ Check if a loss name exists
    """
    return loss_name in _loss_entrypoints


def loss_entrypoint(loss_name):
    """Fetch a loss entrypoint for specified loss name
    """
    return _loss_entrypoints[loss_name]


def list_modules():
    """ Return list of module names that contain loss / loss entrypoints
    """
    modules = _module_to_loss.keys()
    return list(sorted(modules))


def is_loss_in_modules(loss_name, module_names):
    """Check if a loss exists within a subset of modules
    Args:
        loss_name (str) - name of loss to check
        module_names (tuple, list, set) - names of modules to search in
    """
    assert isinstance(module_names, (tuple, list, set))
    return any(loss_name in _module_to_loss[n] for n in module_names)


def has_loss_default_key(loss_name, cfg_key):
    """ Query loss default_cfgs for existence of a specific key.
    """
    if loss_name in _loss_default_cfgs and cfg_key in _loss_default_cfgs[loss_name]:
        return True
    return False


def is_loss_default_key(loss_name, cfg_key):
    """ Return truthy value for specified loss default_cfg key, False if does not exist.
    """
    if loss_name in _loss_default_cfgs and _loss_default_cfgs[loss_name].get(cfg_key, False):
        return True
    return False


def get_loss_default_value(loss_name, cfg_key):
    """ Get a specific loss default_cfg value by key. None if it doesn't exist.
    """
    if loss_name in _loss_default_cfgs:
        return _loss_default_cfgs[loss_name].get(cfg_key, None)
    return None


def is_loss_pretrained(loss_name):
    return loss_name in _loss_has_pretrained
