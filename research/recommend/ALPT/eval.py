# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License Version 2.0(the "License");
# you may not use this file except in compliance with the License.
# you may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0#
#
# Unless required by applicable law or agreed to in writing software
# distributed under the License is distributed on an "AS IS" BASIS
# WITHOUT WARRANT IES OR CONITTONS OF ANY KIND， either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ====================================================================================

from mindspore import Model, context
from mindspore.train.serialization import load_checkpoint, load_param_into_net
from src.deep_and_cross import PredictWithSigmoid, TrainStepWrap, NetWithLossClass, DeepCrossModel, TrainStepWrap_ALPT
from src.callbacks import EvalCallBack
from src.datasets import create_dataset, DataType
from src.metrics import AUCMetric
from src.config import DeepCrossConfig


def get_DCN_net(configure):
    DCN_net = DeepCrossModel(configure)
    loss_net = NetWithLossClass(DCN_net)
    if configure.emb_type == "alpt":
        train_net = TrainStepWrap_ALPT(loss_net)
    else:
        train_net = TrainStepWrap(loss_net)
    eval_net = PredictWithSigmoid(DCN_net)
    return train_net, eval_net

class ModelBuilder():
    """
    Build the model.
    """
    def __init__(self):
        pass

    def get_hook(self):
        pass

    def get_net(self, configure):
        return get_DCN_net(configure)

def test_eval(configure):
    """
    test_eval
    """
    batch_size = configure.batch_size
    field_size = configure.field_size
    ds_eval = create_dataset('./data/mindrecord', train_mode=False, epochs=1,
                             batch_size=batch_size, data_type=DataType.MINDRECORD, target_column=field_size+1)
    print("ds_eval.size: {}".format(ds_eval.get_dataset_size()))
    net_builder = ModelBuilder()
    train_net, eval_net = net_builder.get_net(configure)
    ckpt_path = configure.ckpt_path
    param_dict = load_checkpoint(ckpt_path)
    load_param_into_net(eval_net, param_dict)
    auc_metric = AUCMetric()
    model = Model(train_net, eval_network=eval_net, metrics={"auc": auc_metric})
    eval_callback = EvalCallBack(model, ds_eval, auc_metric, configure)
    model.eval(ds_eval, callbacks=eval_callback)


if __name__ == "__main__":
    config = DeepCrossConfig()
    config.argparse_init()
    # context.set_context(mode=context.GRAPH_MODE, device_target='GPU')
    context.set_context(mode=context.PYNATIVE_MODE, device_target='GPU')
    config.ckpt_path = "./checkpoints/deepcross_train_2-1_4132.ckpt"
    test_eval(config)
