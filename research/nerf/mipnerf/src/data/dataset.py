# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/kakaobrain/nerf-factory
# ============================================================================
"""
Dataset as in nerf-factory.
"""
from typing import Tuple, Optional

import mindspore as ms
import numpy as np

from src.data.llff_loader import load_llff_data
from src.data.blender_loader import load_blender_data
from src.data.ray_utils import batchified_get_rays


class LitData:
    def __init__(
            self,
            data_type: str,
            datadir: str,
            stage: str = 'test',
            # Blender specific
            cam_scale_factor: float = 1.0,
            # LLFF specific arguments
            factor: int = 4,
            llffhold: int = 8,
            spherify: bool = False,
            path_zflat: bool = False,
            offset: int = 250,
            ndc_coord=(-1.0, -1.0),
            chunk: int = 1024 * 32,
            epoch_size: int = 50000,
            use_pixel_centers: bool = True,
            white_bkgd: bool = False,
            scene_center: Tuple[float] = (0.0, 0.0, 0.0),
            scene_radius: Tuple[float] = (1.0, 1.0, 1.0),
            use_sphere_bound: bool = True,
            load_radii: bool = False,
            near: Optional[float] = None,
            far: Optional[float] = None,
    ):
        assert data_type in ['llff', 'blender']

        (
            self.images,
            self.intrinsics,
            self.extrinsics,
            self.image_sizes,
            self.near,
            self.far,
            self.ndc_coeffs,
            (self.i_train, self.i_val, self.i_test, self.i_all),
            self.render_poses,
        ) = \
            load_llff_data(
                datadir=datadir,
                factor=factor,
                ndc_coord=ndc_coord,
                recenter=True,
                bd_factor=0.75,
                spherify=spherify,
                llffhold=llffhold,
                path_zflat=path_zflat,
                near=near,
                far=far) if data_type == 'llff' else \
            load_blender_data(
                datadir=datadir,
                train_skip=1,
                val_skip=1,
                test_skip=1,
                cam_scale_factor=cam_scale_factor,
                white_bkgd=white_bkgd)

        self.normals = None
        self.multlosses = None

        self.width = self.image_sizes[0][1]
        self.height = self.image_sizes[0][0]
        radx = 1 + 2 * offset / self.image_sizes[0][1]
        rady = 1 + 2 * offset / self.image_sizes[0][0]
        radz = 1.0
        self.scene_radius = [radx, rady, radz]
        self.use_sphere_bound = False

        # Get GPU numbers here
        self.num_devices = 1

        self.ndc_coord = ndc_coord
        self.chunk = chunk
        self.epoch_size = epoch_size
        self.use_pixel_centers = use_pixel_centers
        self.white_bkgd = white_bkgd
        self.scene_center = scene_center
        self.scene_radius = scene_radius
        self.use_sphere_bound = use_sphere_bound
        self.load_radii = load_radii

        if stage == "test":
            self.dset, self.dummy = self.split_each(
                self.images, self.normals, None, self.i_test, dummy=True
            )
            self.image_sizes = self.image_sizes[self.i_test]
        if stage == "train":
            self.dset, _ = self.split_each(
                self.images, self.normals, None, self.i_train, dummy=False
            )
            self.image_sizes = self.image_sizes[self.i_train]
        if stage == "val":
            self.dset, self.dummy = self.split_each(
                self.images, self.normals, None, self.i_val, dummy=True
            )
            self.image_sizes = self.image_sizes[self.i_val]
        self.N_total = len(self.dset['rays_o'])

        self.rank = 0
        self.num_replicas = 1
        self.idx_list = np.arange(self.N_total)
        if self.N_total % chunk != 0:
            print(f'Warning! {self.N_total} cannot be divided by chunk size {chunk}')
        self.total_chunks = self.N_total // chunk
        self.chunk_size = chunk

    def split_each(
            self,
            _images,
            _normals,
            render_poses,
            idx,
            dummy=True,
    ):
        images = None
        normals = None
        radii = None
        multloss = None

        if _images is not None:
            extrinsics_idx = self.extrinsics[idx]
            intrinsics_idx = self.intrinsics[idx]
            image_sizes_idx = self.image_sizes[idx]
        else:
            extrinsics_idx = render_poses
            N_render = len(render_poses)
            intrinsics_idx = np.stack([self.intrinsics[0] for _ in range(N_render)])
            image_sizes_idx = np.stack([self.image_sizes[0] for _ in range(N_render)])

        _rays_o, _rays_d, _viewdirs, _radii, _multloss = batchified_get_rays(
            intrinsics_idx,
            extrinsics_idx,
            image_sizes_idx,
            self.use_pixel_centers,
            self.load_radii,
            self.ndc_coord,
            self.ndc_coeffs,
            self.multlosses[idx] if self.multlosses is not None else None,
        )

        device_count = self.num_devices
        n_dset = len(_rays_o)
        dummy_num = (
            (device_count - n_dset % device_count) % device_count if dummy else 0
        )

        rays_o = np.zeros((n_dset + dummy_num, 3), dtype=np.float32)
        rays_d = np.zeros((n_dset + dummy_num, 3), dtype=np.float32)
        viewdirs = np.zeros((n_dset + dummy_num, 3), dtype=np.float32)

        rays_o[:n_dset], rays_o[n_dset:] = _rays_o, _rays_o[:dummy_num]
        rays_d[:n_dset], rays_d[n_dset:] = _rays_d, _rays_d[:dummy_num]
        viewdirs[:n_dset], viewdirs[n_dset:] = _viewdirs, _viewdirs[:dummy_num]

        viewdirs = viewdirs / np.linalg.norm(viewdirs, axis=1, keepdims=True)

        if _images is not None:
            images_idx = np.concatenate([_images[i].reshape(-1, 3) for i in idx])
            images = np.zeros((n_dset + dummy_num, 3))
            images[:n_dset] = images_idx
            images[n_dset:] = images[:dummy_num]

        if _normals is not None:
            normals_idx = np.concatenate([_normals[i].reshape(-1, 4) for i in idx])
            normals = np.zeros((n_dset + dummy_num, 4))
            normals[:n_dset] = normals_idx
            normals[n_dset:] = normals[:dummy_num]

        if _radii is not None:
            radii = np.zeros((n_dset + dummy_num, 1), dtype=np.float32)
            radii[:n_dset], radii[n_dset:] = _radii, _radii[:dummy_num]

        if _multloss is not None:
            multloss = np.zeros((n_dset + dummy_num, 1), dtype=np.float32)
            multloss[:n_dset], multloss[n_dset:] = _multloss, _multloss[:dummy_num]

        rays_info = {
            "rays_o": rays_o,
            "rays_d": rays_d,
            "viewdirs": viewdirs,
            "images": images,
            "radii": radii,
            "multloss": multloss,
            "normals": normals,
        }

        return rays_info, dummy_num

    def __getitem__(self, item):
        """Item means chunk i. An image is splits to many chunks."""
        start = self.chunk_size * item
        end = self.chunk_size * (item + 1)

        target = self.dset['images'][start: end].astype(np.float32)
        rays_o = self.dset['rays_o'][start: end].astype(np.float32)
        rays_d = self.dset['rays_d'][start: end].astype(np.float32)
        raw_rays_d = self.dset['viewdirs'][start: end].astype(np.float32)
        radii = self.dset['radii'][start: end].astype(np.float32)
        rays = np.concatenate((rays_o, rays_d, raw_rays_d, radii), axis=-1)
        return rays, target

    def __len__(self):
        return self.total_chunks


def create_datasets(data_path: str,
                    data_cfg,
                    train_rays_chunk_size: int,
                    val_rays_chunk_size: int,
                    train_shuffle: bool = True,
                    num_parallel_workers: int = 1,
                    num_shards: int = 1,
                    shard_id: int = 0):

    train_ds = LitData(data_cfg['data_type'],
                       data_path, stage='train',
                       white_bkgd=data_cfg['white_bkgd'],
                       ndc_coord=data_cfg['ndc_coord'],
                       load_radii=data_cfg['load_radii'],
                       use_pixel_centers=data_cfg['use_pixel_centers'],
                       chunk=train_rays_chunk_size,
                       epoch_size=data_cfg['epoch_size']
                       )

    scene_params = {
        'in_ndc': data_cfg['ndc_coord'],
        'near': train_ds.near,
        'far': train_ds.far,
        'height': train_ds.height,
        'width': train_ds.width,
        'white_background': data_cfg['white_bkgd']
    }
    train_ds = ms.dataset.GeneratorDataset(
        source=train_ds,
        column_names=['rays', 'target'],
        column_types=[ms.float32, ms.float32],
        shuffle=train_shuffle,
        num_parallel_workers=num_parallel_workers,
        num_shards=num_shards,
        shard_id=shard_id,
    )

    val_ds = LitData(data_cfg['data_type'],
                     data_path, stage='val',
                     white_bkgd=data_cfg['white_bkgd'],
                     ndc_coord=data_cfg['ndc_coord'],
                     load_radii=data_cfg['load_radii'],
                     use_pixel_centers=data_cfg['use_pixel_centers'],
                     chunk=val_rays_chunk_size,
                     epoch_size=data_cfg['epoch_size']
                     )
    val_ds = ms.dataset.GeneratorDataset(
        source=val_ds,
        column_names=['rays', 'target'],
        column_types=[ms.float32, ms.float32],
        shuffle=False,
        num_parallel_workers=num_parallel_workers)
    return train_ds, val_ds, scene_params
