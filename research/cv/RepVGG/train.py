# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Train RepVGG on ImageNet."""
import shutil
import subprocess
import sys
import traceback

from datetime import datetime
from functools import reduce
from pathlib import Path

from mindspore import Model
from mindspore import context
from mindspore import nn
from mindspore import dataset as ds
from mindspore.common import set_seed
from mindspore.train.callback import (
    CheckpointConfig, ModelCheckpoint, LossMonitor
)

from src.tools.callback import (
    BestCheckpointSavingCallback, SummaryCallbackWithEval,
    TrainTimeMonitor,
    EvalTimeMonitor
)
from src.tools.optimizer import get_optimizer
from src.tools.amp import cast_amp
from src.tools.criterion import get_criterion, NetWithLoss
from src.tools.utils import set_device, pretrained, get_train_one_step
from src.dataset import get_dataset
from src.repvgg import get_model


def get_callbacks(
        arch, rank, train_data_size, val_data_size, ckpt_dir, best_ckpt_dir,
        summary_dir, ckpt_save_every_step=0, ckpt_save_every_sec=0,
        ckpt_keep_num=10, best_ckpt_num=5, print_loss_every=1, collect_freq=1,
        collect_tensor_freq=10, collect_input_data=False,
        keep_default_action=False
):
    """Get common callbacks."""
    if ckpt_save_every_step == 0 and ckpt_save_every_sec == 0:
        ckpt_save_every_step = train_data_size
    config_ck = CheckpointConfig(
        save_checkpoint_steps=ckpt_save_every_step,
        save_checkpoint_seconds=ckpt_save_every_sec,
        keep_checkpoint_max=ckpt_keep_num,
        append_info=['epoch_num', 'step_num']
    )
    train_time_cb = TrainTimeMonitor(data_size=train_data_size)
    eval_time_cb = EvalTimeMonitor(data_size=val_data_size)

    best_ckpt_save_cb = BestCheckpointSavingCallback(
        best_ckpt_dir, prefix=arch, buffer=best_ckpt_num
    )

    ckpoint_cb = ModelCheckpoint(
        prefix=f'{arch}_{rank}',
        directory=ckpt_dir,
        config=config_ck
    )
    loss_cb = LossMonitor(print_loss_every)

    specified = {
        'collect_metric': True,
        'collect_train_lineage': True,
        'collect_eval_lineage': True,
        'collect_input_data': collect_input_data,
    }
    summary_collector_cb = SummaryCallbackWithEval(
        summary_dir=summary_dir,
        collect_specified_data=specified,
        collect_freq=collect_freq,
        keep_default_action=keep_default_action,
        collect_tensor_freq=collect_tensor_freq
    )
    return [
        train_time_cb,
        eval_time_cb,
        ckpoint_cb,
        loss_cb,
        best_ckpt_save_cb,
        summary_collector_cb
    ]


def dump_env_and_params(ckpt_save_dir, args):
    """Dump information about environment ang hyper parameters."""
    shutil.copy(str(args.config), str(ckpt_save_dir))
    with open(str(ckpt_save_dir / 'cmd.txt'), 'w', encoding='utf-8'
              ) as file:
        file.write(' '.join(sys.argv))
    with open(str(ckpt_save_dir / 'args.txt'), 'w', encoding='utf-8'
              ) as file:
        file.write(str(args))
    try:
        with open(str(ckpt_save_dir / 'git.txt'), 'w', encoding='utf-8'
                  ) as file:
            commit_info = subprocess.check_output(
                ['git', 'show', '-s'],
                cwd=Path(__file__).absolute().parents[0],
            )
            decoded_commit_info = commit_info.decode('utf-8')
            decoded_commit_info = decoded_commit_info.replace('\n', ', ')
            file.write(decoded_commit_info)
    except subprocess.CalledProcessError as git_exception:
        print(f'Git dumping error: {git_exception}')
        print(traceback.format_exc())


def main():
    """Entry point."""
    from src.config import run_args
    args = run_args()

    print(args)
    set_seed(args.seed)
    context.set_context(
        mode=context.GRAPH_MODE, device_target=args.device_target,
        enable_graph_kernel=False
    )
    if args.device_target == 'Ascend':
        context.set_context(enable_auto_mixed_precision=True)
    if args.device_target != 'CPU':
        rank = set_device(args)
    else:
        rank = 0

    ds.config.set_prefetch_size(args.prefetch)

    # get model and cast amp_level
    net = get_model(args)
    cast_amp(net, args)
    criterion = get_criterion(args)
    net_with_loss = NetWithLoss(net, criterion)

    data = get_dataset(args)
    batch_num = data.train_dataset.get_dataset_size()
    optimizer = get_optimizer(args, net, batch_num)

    net_with_loss = get_train_one_step(args, net_with_loss, optimizer)
    if args.pretrained:
        pretrained(args, net_with_loss, args.exclude_epoch_state)

    eval_network = nn.WithEvalCell(
        net, criterion, args.amp_level in ['O2', 'O3', 'auto']
    )
    eval_indexes = [0, 1, 2]
    model = Model(
        net_with_loss, metrics={'acc', 'loss'}, eval_network=eval_network,
        eval_indexes=eval_indexes
    )

    # target folder path
    experiment_name = '_'.join([
        datetime.now().strftime('%y%m%d_%H%M%S'), args.arch, str(rank),
    ])
    if args.brief is not None:
        experiment_name = f'{experiment_name}_{args.brief}'
    if args.continues is None:
        ckpt_save_dir = args.train_url / experiment_name
    else:
        ckpt_save_dir = args.continues

    callbacks = [
        TrainTimeMonitor(data_size=data.train_dataset.get_dataset_size()),
        LossMonitor(args.save_every)
    ]
    if rank == 0:
        callbacks = get_callbacks(
            arch=args.arch, rank=rank, ckpt_dir=str(ckpt_save_dir),
            train_data_size=data.train_dataset.get_dataset_size(),
            val_data_size=data.val_dataset.get_dataset_size(),
            best_ckpt_dir=str(ckpt_save_dir / 'best_acc'),
            summary_dir=str(ckpt_save_dir / 'logs'), ckpt_save_every_sec=0,
            ckpt_save_every_step=args.save_every,
            print_loss_every=args.save_every,
            ckpt_keep_num=args.keep_checkpoint_max,
            best_ckpt_num=args.keep_best_checkpoints_max
        )

        dump_env_and_params(ckpt_save_dir, args)

    print(
        'Number of parameters:',
        sum(
            reduce(lambda x, y: x * y, params.shape)
            for params in net.trainable_params()
        )
    )
    print('begin train')
    model.fit(
        args.epochs, data.train_dataset, valid_dataset=data.val_dataset,
        dataset_sink_mode=bool(args.use_data_sink), callbacks=callbacks,
        initial_epoch=args.start_epoch
    )
    print('train success')


if __name__ == '__main__':
    main()
