# Copyright 2020-2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""GridRcnn proposal generator."""
import numpy as np
import mindspore as ms

from mindspore import ops
from mindspore import nn
from mindspore import Tensor

class Proposal(nn.Cell):
    """
    Proposal subnet.

    Args:
        config (dict): Config.
        batch_size (int): Batchsize.
        num_classes (int) - Class number.
        use_sigmoid_cls (bool) - Select sigmoid or softmax function.
        target_means (tuple) - Means for encode function. Default: (.0, .0, .0, .0).
        target_stds (tuple) - Stds for encode function. Default: (1.0, 1.0, 1.0, 1.0).

    Returns:
        Tuple, tuple of output tensor,(proposal, mask).

    Examples:
        Proposal(config = config, batch_size = 1, num_classes = 81, use_sigmoid_cls = True, \
                 target_means=(.0, .0, .0, .0), target_stds=(1.0, 1.0, 1.0, 1.0))
    """
    def __init__(
            self,
            config,
            batch_size,
            num_classes,
            use_sigmoid_cls,
            target_means=(.0, .0, .0, .0),
            target_stds=(1.0, 1.0, 1.0, 1.0)
    ):
        super().__init__()
        cfg = config
        self.batch_size = batch_size
        self.num_classes = num_classes
        self.target_means = target_means
        self.target_stds = target_stds
        self.means_tensor = ms.Tensor(target_means).reshape(-1, 4)
        self.stds_tensor = ms.Tensor(target_stds).reshape(-1, 4)
        self.wh_ratio_clip = ms.Tensor(16 / 1000)
        self.img_shape = (cfg.img_height, cfg.img_width)
        self.use_sigmoid_cls = use_sigmoid_cls

        if self.use_sigmoid_cls:
            self.cls_out_channels = num_classes - 1
            self.activation = ops.Sigmoid()
            self.reshape_shape = (-1, 1)
        else:
            self.cls_out_channels = num_classes
            self.activation = ops.Softmax(axis=1)
            self.reshape_shape = (-1, 2)

        if self.cls_out_channels <= 0:
            raise ValueError(f'num_classes={num_classes} is too small')

        self.num_pre = cfg.rpn_proposal_nms_pre
        self.min_box_size = cfg.rpn_proposal_min_bbox_size
        self.nms_thr = cfg.rpn_proposal_nms_thr
        self.nms_post = cfg.rpn_proposal_nms_post
        self.nms_across_levels = cfg.rpn_proposal_nms_across_levels
        self.max_num = cfg.rpn_proposal_max_num
        self.num_levels = cfg.fpn_num_outs

        # Op Define
        self.squeeze = ops.Squeeze()
        self.reshape = ops.Reshape()
        self.cast = ops.Cast()

        self.feature_shapes = cfg.feature_shapes

        self.transpose_shape = (1, 2, 0)

        self.nms = ops.NMSWithMask(self.nms_thr)
        self.concat_axis0 = ops.Concat(axis=0)
        self.concat_axis1 = ops.Concat(axis=1)
        self.split = ops.Split(axis=1, output_num=5)
        self.min = ops.Minimum()
        self.gatherND = ops.GatherNd()
        self.slice = ops.Slice()
        self.select = ops.Select()
        self.greater = ops.Greater()
        self.transpose = ops.Transpose()
        self.tile = ops.Tile()
        self.set_train_local(config, training=True)

        self.dtype = np.float32
        self.ms_type = ms.float32

        self.multi_10 = Tensor(10.0, self.ms_type)

    def set_train_local(self, config, training=True):
        """Set training flag."""
        self.training_local = training

        cfg = config
        self.topK_stage1 = ()
        self.topK_shape = ()
        total_max_topk_input = 0
        if not self.training_local:
            self.num_pre = cfg.rpn_nms_pre
            self.min_box_size = cfg.rpn_proposal_min_bbox_size
            self.nms_thr = cfg.rpn_nms_thr
            self.nms_post = cfg.rpn_nms_post
            self.nms_across_levels = cfg.rpn_nms_across_levels
            self.max_num = cfg.rpn_max_num

        for shp in self.feature_shapes:
            k_num = min(self.num_pre, (shp[0] * shp[1] * 3))
            total_max_topk_input += k_num
            self.topK_stage1 += (k_num,)
            self.topK_shape += ((k_num, 1),)

        self.topKv2 = ops.TopK(sorted=True)
        self.topK_shape_stage2 = (self.max_num, 1)
        self.min_float_num = -65500.0
        self.topK_mask = Tensor(self.min_float_num * np.ones(total_max_topk_input, np.float32))

    def construct(self, rpn_cls_score_total, rpn_bbox_pred_total, anchor_list):
        proposals_tuple = ()
        masks_tuple = ()
        for img_id in range(self.batch_size):
            cls_score_list = ()
            bbox_pred_list = ()
            for i in range(self.num_levels):
                rpn_cls_score_i = self.squeeze(rpn_cls_score_total[i][img_id:img_id+1:1, ::, ::, ::])
                rpn_bbox_pred_i = self.squeeze(rpn_bbox_pred_total[i][img_id:img_id+1:1, ::, ::, ::])

                cls_score_list = cls_score_list + (rpn_cls_score_i,)
                bbox_pred_list = bbox_pred_list + (rpn_bbox_pred_i,)

            proposals, masks = self.get_bboxes_single(cls_score_list, bbox_pred_list, anchor_list)
            proposals_tuple += (proposals,)
            masks_tuple += (masks,)
        return proposals_tuple, masks_tuple

    def get_bboxes_single(self, cls_scores, bbox_preds, mlvl_anchors):
        """Get proposal boundingbox."""
        mlvl_proposals = ()
        mlvl_mask = ()
        for idx in range(self.num_levels):
            rpn_cls_score = self.transpose(cls_scores[idx], self.transpose_shape)
            rpn_bbox_pred = self.transpose(bbox_preds[idx], self.transpose_shape)
            anchors = mlvl_anchors[idx]

            rpn_cls_score = self.reshape(rpn_cls_score, self.reshape_shape)
            rpn_cls_score = self.activation(rpn_cls_score)
            rpn_cls_score_process = self.cast(self.squeeze(rpn_cls_score[::, 0::]), self.ms_type)

            rpn_bbox_pred_process = self.cast(self.reshape(rpn_bbox_pred, (-1, 4)), self.ms_type)

            scores = rpn_cls_score_process
            bboxes = rpn_bbox_pred_process

            if self.num_pre > 0 and rpn_cls_score.shape[0] > self.num_pre:
                scores_sorted, topk_inds = self.topKv2(rpn_cls_score_process, self.topK_stage1[idx])

                topk_inds = self.reshape(topk_inds, self.topK_shape[idx])

                bboxes_sorted = self.gatherND(rpn_bbox_pred_process, topk_inds)
                anchors_sorted = self.cast(self.gatherND(anchors, topk_inds), self.ms_type)
                scores_sorted = self.reshape(scores_sorted, self.topK_shape[idx])
                anchors, scores, bboxes = anchors_sorted, scores_sorted, bboxes_sorted
            else:
                scores = self.reshape(scores, (-1, 1))
            proposals_decode = self.delta2bbox(anchors, bboxes)

            proposals_decode = self.concat_axis1((proposals_decode, scores))
            proposals, _, mask_valid = self.nms(proposals_decode)
            mlvl_proposals = mlvl_proposals + (proposals,)
            mlvl_mask = mlvl_mask + (mask_valid,)

        proposals = self.concat_axis0(mlvl_proposals)
        masks = self.concat_axis0(mlvl_mask)

        _, _, _, _, scores = self.split(proposals)
        scores = self.squeeze(scores)
        topk_mask = self.cast(self.topK_mask, self.ms_type)
        scores_using = self.select(masks, scores, topk_mask)

        _, topk_inds = self.topKv2(scores_using, self.max_num)

        topk_inds = self.reshape(topk_inds, self.topK_shape_stage2)
        proposals = self.gatherND(proposals, topk_inds)
        masks = self.gatherND(masks, topk_inds)
        return proposals, masks

    def delta2bbox(self, rois, deltas):
        denorm_deltas = deltas * self.stds_tensor + self.means_tensor
        dx = denorm_deltas[:, 0:1]
        dy = denorm_deltas[:, 1:2]
        dw = denorm_deltas[:, 2:3]
        dh = denorm_deltas[:, 3:4]
        max_ratio = ms.ops.abs(ms.ops.log(self.wh_ratio_clip))
        dw = ms.ops.clip_by_value(
            dw, clip_value_min=-max_ratio, clip_value_max=max_ratio
        )
        dh = ms.ops.clip_by_value(
            dh, clip_value_min=-max_ratio, clip_value_max=max_ratio
        )
        px = ((rois[:, 0] + rois[:, 2]) * 0.5).reshape(-1, 1)
        py = ((rois[:, 1] + rois[:, 3]) * 0.5).reshape(-1, 1)
        pw = (rois[:, 2] - rois[:, 0] + 1.0).reshape(-1, 1)
        ph = (rois[:, 3] - rois[:, 1] + 1.0).reshape(-1, 1)
        gw = pw * dw.exp()
        gh = ph * dh.exp()
        gx = px + pw * dx  # gx = px + pw * dx
        gy = py + ph * dy  # gy = py + ph * dy
        x1 = gx - gw * 0.5 + 0.5
        y1 = gy - gh * 0.5 + 0.5
        x2 = gx + gw * 0.5 - 0.5
        y2 = gy + gh * 0.5 - 0.5

        x1 = ms.ops.clip_by_value(
            x1, clip_value_min=0, clip_value_max=self.img_shape[1] - 1
        )
        y1 = ms.ops.clip_by_value(
            y1, clip_value_min=0, clip_value_max=self.img_shape[0] - 1
        )
        x2 = ms.ops.clip_by_value(
            x2, clip_value_min=0, clip_value_max=self.img_shape[1] - 1
        )
        y2 = ms.ops.clip_by_value(
            y2, clip_value_min=0, clip_value_max=self.img_shape[0] - 1
        )
        bboxes = ms.ops.concat([x1, y1, x2, y2], axis=-1).reshape(-1, 4)
        return bboxes
